# -*- coding: utf-8 -*-
from django.db.models import Q
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework_jwt.utils import jwt_decode_handler

from user_management.models import User
from .models import FCMDevice

class FCMDeviceSerializer(serializers.ModelSerializer):
    token = serializers.CharField(max_length=400, required=True)
    class Meta:
        model = FCMDevice
        fields = ['registration_id', 'id', 'type', 'token']
        read_only_fields = ['id']

    def validate(self, attrs):
        try:
            token = jwt_decode_handler(attrs.pop('token'))
        except:
            raise ValidationError('توکن معتبر نمی‌باشد.')
        user_id = token['user_id']
        user = User.objects.filter(id=user_id).first()
        attrs['user'] = user
        fcm_token = attrs['registration_id']
        FCMDevice.objects.filter(Q(registration_id=fcm_token, type=attrs['type'])| Q(user=user, type=attrs['type'])).delete()
        return attrs

    def create(self, validated_data):
        fcm_obj = FCMDevice.objects.create(**validated_data)
        return fcm_obj

