# -*- coding: utf-8 -*-
from rest_framework.viewsets import GenericViewSet
from rest_framework.decorators import action
from rest_framework_jwt.utils import jwt_decode_handler

from .permissions import IsOwner
from user_management.responses import CustomResponse
from .serializers import *

class FCMDeviceViewSet(GenericViewSet):
    queryset = FCMDevice.objects.all()
    serializer_class = FCMDeviceSerializer
    lookup_field = 'registration_id'

    @action(methods=['post'], url_path='create', detail=False)
    def create_token(self, request, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return CustomResponse()

    @action(methods=['delete'], url_path='delete', detail=True, permission_classes=[IsOwner])
    def delete_token(self, request, pk, **kwargs):
        token = self.get_object()
        token.delete()
        return CustomResponse()