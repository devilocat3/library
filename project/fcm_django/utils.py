# -*- coding: utf-8 -*-
from django.core.mail import send_mail

from .models import FCMDevice


def send_notification_to_user(user, title, msg, type):
    user_devices = FCMDevice.objects.filter(user=user)
    for device in user_devices:
        if device.type == 'web':
            device.send_message(title=title, body=msg, data={"type": type})
        else:
            device.send_message(data={"type": type, "message": msg})
    if user.email:
        send_mail(title, msg, 'devilocat3@gmail.com', [user.email])
    return True
