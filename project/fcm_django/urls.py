# -*- coding: utf-8 -*-
from rest_framework.routers import DefaultRouter
from django.conf.urls import url, include

from .views import FCMDeviceViewSet

router = DefaultRouter()
router.register(r'devices', FCMDeviceViewSet)

urlpatterns = [
    url(r'', include(router.urls))
]