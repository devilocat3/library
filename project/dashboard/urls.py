from rest_framework.routers import DefaultRouter
from django.conf.urls import url, include
from .views import DashboardStatisticsViewset

router = DefaultRouter()
router.register(r'statistics', DashboardStatisticsViewset)


urlpatterns = [
    url('', include(router.urls)),
]