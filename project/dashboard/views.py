# -*- coding: utf-8 -*-
import jdatetime
import datetime
from rest_framework.viewsets import GenericViewSet
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from user_management.responses import CustomResponse
from user_management.permissions import IsAdminOrHasPermission
from user_management.models import User
from book.models import Book, Maghale, Tarh, UserBook
from .serializers import StatisticsDateRangeSerializer
from jalali import convert_string_to_jdate


class DashboardStatisticsViewset(GenericViewSet):
    queryset = User.objects.all()
    permission_classes = [IsAdminOrHasPermission]
    needed_permissions = ['manage_logs', 'view_logs']
    serializer_class = StatisticsDateRangeSerializer

    def list(self, request, **kwargs):
        """
        Returns the details about today such as number of today registrations, added books, reserved books and etc
        :param request:
        :param kwargs:
        :return:
        """
        today = jdatetime.datetime.today()
        today = jdatetime.datetime(today.year, today.month, today.day)
        today_gregorian = datetime.datetime.today()
        today_gregorian = datetime.datetime(today_gregorian.year, today_gregorian.month, today_gregorian.day)
        today_registered_users = User.objects.filter(date_joined__gte=today, is_staff=False).count()
        today_books = Book.objects.filter(created__gte=today_gregorian).count()
        today_articles = Maghale.objects.filter(created__gte=today).count()
        today_plans = Tarh.objects.filter(created__gte=today).count()
        today_reserves = UserBook.objects.filter(borrow_start_date__gte=today, status='approved').count()
        today_book_returned = UserBook.objects.filter(borrow_return_date__gte=today, status='returned').count()
        data = {
            'registered_users': today_registered_users,
            'added_books': today_books,
            'added_articles': today_articles,
            'added_plans': today_plans,
            'book_returned': today_book_returned,
            'approved_reserves': today_reserves,
        }

        return CustomResponse(data)

    @action(methods=['post'], url_path='general', detail=False)
    def get_general_info(self, request, **kwargs):
        """
        Gets a date range(from_date && to_date) and returns this range statistics.
        :param request:
        :param kwargs:
        :return:
        """
        date_to = convert_string_to_jdate(request.data['to_date'])
        date_from = convert_string_to_jdate(request.data['from_date'])
        date_to_gregorian = date_to.togregorian()
        date_from_gregorian = date_from.togregorian()
        reg_users = User.objects.filter(date_joined__lte=date_to).filter(date_joined__gte=date_from).count()
        books = Book.objects.filter(created__gte=date_from_gregorian).filter(created__lte=date_to_gregorian).count()
        articles = Maghale.objects.filter(created__lte=date_to).filter(created__gte=date_from).count()
        plans = Tarh.objects.filter(created__lte=date_to).filter(created__gte=date_from).count()
        book_returned = UserBook.objects.filter(borrow_return_date__lte=date_to, status='returned').filter(
            borrow_return_date__gte=date_from).count()
        reserves = UserBook.objects.filter(borrow_start_date__lte=date_to, status='approved').filter(
            borrow_start_date__gte=date_from).count()
        total_inactive_users = User.objects.filter(is_active=False).count()
        data = {
            'registered_users': reg_users,
            'added_books': books,
            'added_articles': articles,
            'added_plans': plans,
            'book_returned': book_returned,
            'approved_reserves': reserves,
            'total_inactive_users': total_inactive_users,
        }

        return CustomResponse(data)

    @action(methods=['get'], url_path='full/info', detail=False, permission_classes=[IsAuthenticated])
    def get_general_info_counts(self, request, **kwargs):
        books = Book.objects.all().count()
        articles = Maghale.objects.all().count()
        plans = Tarh.objects.all().count()
        data = {
            'total_books': books,
            'total_articles': articles,
            'total_plans': plans,
        }
        return CustomResponse(data)