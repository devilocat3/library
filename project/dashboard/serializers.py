# -*- coding: utf-8 -*-
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
import jdatetime

class StatisticsDateRangeSerializer(serializers.Serializer):
    from_date = serializers.RegexField('\d{4}\-\d{2}\-\d{2}')
    to_date = serializers.RegexField('\d{4}\-\d{2}\-\d{2}')

    def validate(self, attrs):
        if attrs.get('from_date') > attrs.get('to_date'):
            raise ValidationError(u'تاریخ شروع باید کوچک‌تر از تاریخ پایان باشد.')
        try:
            attrs['from_date'] =jdatetime.datetime.strptime(attrs.get('from_date'), '%Y-%m-%d')
            attrs['to_date'] =jdatetime.datetime.strptime(attrs.get('to_date'), '%Y-%m-%d')
        except:
            raise ValidationError('تاریخ وارده معتبر نمی‌باشد.')
        return attrs
