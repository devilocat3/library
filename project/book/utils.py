# -*- coding: utf-8 -*-
from PyZ3950 import zoom
from lxml import etree
from rest_framework.exceptions import ValidationError
import re
from .models import UserBook, Book
from fcm_django.models import FCMDevice


class LibReader:
    database = 'default'
    address = 'z3950.nlai.ir'
    port = 210
    preferred_syntax = 'XML'
    search_type = 'PQF'
    connection = None

    def __init__(self):
        self.make_connection()

    def make_connection(self):
        try:
            self.connection = zoom.Connection(self.address, self.port)
        except:
            raise ValidationError("در حال حاضر امکان ارتباط با کتابخانه مرکزی وجود ندارد.")
        self.connection.preferredRecordSyntax = self.preferred_syntax
        self.connection.databaseName = self.database

    def search_isbn(self, isbn):
        if not self.connection:
            raise ValidationError('ارتباط برقرار نیست')
        query_str = '@attr 1=7 {}'.format(isbn)
        query = zoom.Query(self.search_type, query_str)

        try:
            response = self.connection.search(query)
        except:
            raise ValidationError('جست‌و‌جو با خطا مواجه شد.')
        if response._searchResult.resultCount == 0:
            raise ValidationError("کتاب پیدا نشد.")
        data = response[0].data
        self.connection.close()
        return data


class Parser:
    xml_data = None

    def convert(self, data, type='JSON'):
        if type == 'JSON':
            response = self._convert_to_json(data)
        else:
            response = self._convert_to_xml(data)
        return response

    def _convert_to_xml(self, data):
        pass

    def _convert_to_json(self, data):
        self.json_response = {}
        try:
            tree = etree.fromstring(data)
        except:
            data = re.sub('[&#]', '', data)
            tree = etree.fromstring(data)
        datafields = tree.findall('datafield')
        for record in tree.findall('controlfield'):
            if record.get('tag') == '001':
                self.json_response.update({'record_id': record.text})
        for record in datafields:
            self.json_response.update(self.switcher(record))
        return self.json_response

    def tag_010(self, record):
        old = self.json_response.get('shabak', [])
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'shabak': child.text})
            elif code == 'b':
                data.update({'vijegiha': child.text})
            elif code == 'd':
                data.update({'sharayet_tahie': child.text})
            elif code == 'z':
                data.update({'wrong_shabak': child.text})
        old.append(data)
        response_json = {'shabak': old}
        return response_json

    @staticmethod
    def tag_020(record):
        children = record.getchildren()
        response_json = {}
        for child in children:
            code = child.get('code')
            if code == 'b':
                response_json.update({'shomare_madrak': {
                    'shomare_madrak': child.text
                }})

        return response_json

    @staticmethod
    def tag_100(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'dade_koli_pardazesh': child.text})
        response_json = {'dade_koli_pardazesh': data}
        return response_json

    @staticmethod
    def tag_101(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'zaban_matn': child.text})
            elif code == 'b':
                data.update({'zaban_asar_asli': child.text})
        response_json = {'zaban_madrak': data}
        return response_json

    @staticmethod
    def tag_200(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'onvan_asli': child.text})
            elif code == 'b':
                data.update({'nam_am_mavad': child.text})
            elif code == 'd':
                data.update({'onvan_movazi': child.text})
            elif code == 'e':
                data.update({'onvan_fari': child.text})
            elif code == 'f':
                data.update({'nam_nevisande': child.text})
            elif code == 'g':
                data.update({'virayeshgar': child.text})
        response_json = {'onvan_padidavarande': data}
        return response_json

    @staticmethod
    def tag_205(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'vaziat_virast': child.text})
            elif code == 'f':
                data.update({'nam_avalin_virastar': child.text})
            elif code == 'g':
                data.update({'nam_sayer_virastar': child.text})
        response_json = {'vaziat_virast': data}
        return response_json

    @staticmethod
    def tag_210(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'makan': child.text})
            elif code == 'c':
                data.update({'esme_nasher': child.text})
            elif code == 'd':
                data.update({'tarikh': child.text})
        response_json = {'nasher_specs': data}
        return response_json

    @staticmethod
    def tag_215(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'shomare_safe': child.text})
            elif code == 'c':
                data.update({'sayer_joziat': child.text})
            elif code == 'd':
                data.update({'abad': child.text})
        response_json = {'moshakhasat_zaheri': data}
        return response_json

    @staticmethod
    def tag_225(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'onvan_froset': child.text})
            elif code == 'e':
                data.update({'onvan_fari_froset': child.text})
            elif code == 'v':
                data.update({'shomare_froset': child.text})
        response_json = {'froset': data}
        return response_json

    @staticmethod
    def tag_300(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'matn_yaddasht': child.text})
        response_json = {'yaddasht_koli': data}
        return response_json

    @staticmethod
    def tag_304(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'matn_yaddasht': child.text})
        response_json = {'yaddasht_onvan_padidavarande': data}
        return response_json

    def tag_305(self, record):
        old = self.json_response.get('yaddasht_virast_tarikhche', [])
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'matn_yaddasht': child.text})
        old.append(data)
        response_json = {'yaddasht_virast_tarikhche': old}
        return response_json

    @staticmethod
    def tag_306(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'matn_yaddasht': child.text})
        response_json = {'yaddasht_nashr_pakhsh': data}
        return response_json

    @staticmethod
    def tag_307(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'matn_yaddasht': child.text})
        response_json = {'yaddasht_moshakhasat_zaheri': data}
        return response_json

    @staticmethod
    def tag_308(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'matn_yaddasht': child.text})
        response_json = {'yaddasht_froset': data}
        return response_json

    @staticmethod
    def tag_312(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'matn_yaddasht': child.text})
        response_json = {'yaddasht_onvanhaye_mortabet': data}
        return response_json

    @staticmethod
    def tag_320(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'matn_yaddasht': child.text})
        response_json = {'ketabname_vajename': data}
        return response_json

    @staticmethod
    def tag_327(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'matn_yaddasht': child.text})
        response_json = {'mondarejat': data}
        return response_json

    @staticmethod
    def tag_330(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'matn_yaddasht': child.text})
        response_json = {'yaddasht_kholase_chekide': data}
        return response_json

    @staticmethod
    def tag_500(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'onvan_gharardadi': child.text})
            elif code == '9':
                data.update({'onvan_gharardadi_taid_nashode': child.text})
        response_json = {'onvan_gharardadi': data}
        return response_json

    @staticmethod
    def tag_510(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'onvan_zaban_digar': child.text})
            elif code == 'e':
                data.update({'onvan_fari': child.text})
        response_json = {'onvan_asli_zaban_digar': data}
        return response_json

    @staticmethod
    def tag_512(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'onvan_ro_jeld': child.text})
            elif code == 'e':
                data.update({'onvan_fari': child.text})

        response_json = {'onvan_ro_jeld': data}
        return response_json

    @staticmethod
    def tag_516(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'onvan_atf': child.text})
            elif code == 'e':
                data.update({'onvan_fari': child.text})
        response_json = {'onvan_atf': data}
        return response_json

    @staticmethod
    def tag_517(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'onvan_gone_gon': child.text})
            elif code == 'e':
                data.update({'onvan_fari': child.text})
        response_json = {'onvan_digar': data}
        return response_json

    @staticmethod
    def tag_600(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'nam_khanevadegi': child.text})
            elif code == 'b':
                data.update({'nam': child.text})
            elif code == 'f':
                data.update({'tarikh_tavalod_vafat': child.text})
            elif code == 'j':
                data.update({'taghsim_fari_shekl': child.text})
            elif code == 'x':
                data.update({'taghsim_fari_mozoie': child.text})
            elif code == 'y':
                data.update({'taghsim_far_joghrafiaie': child.text})
            elif code == 'z':
                data.update({'taghsim_doreie': child.text})
            elif code == '9':
                data.update({'nam_mostanad_nashode': child.text})
        response_json = {'nam_shakhs_manzale_mozo': data}
        return response_json

    @staticmethod
    def tag_601(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'nam_sazman': child.text})
            elif code == 'd':
                data.update({'shomare_hamayesh': child.text})
            elif code == 'e':
                data.update({'mahal_hamayesh': child.text})
            elif code == 'f':
                data.update({'tarikh_hamayesh': child.text})
            elif code == 'j':
                data.update({'taghsim_fari_shekli': child.text})
            elif code == 'x':
                data.update({'taghsim_fari_mozoie': child.text})
            elif code == 'y':
                data.update({'taghsim_far_joghrafiaie': child.text})
            elif code == 'z':
                data.update({'taghsim_doreie': child.text})
            elif code == '9':
                data.update({'nam_mostanad_nashode': child.text})
        response_json = {'nam_tanalegan_manzale_mozo': data}
        return response_json

    @staticmethod
    def tag_605(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'onsor_shenaseie': child.text})
            elif code == 'e':
                data.update({'taghsim_fari_shekli': child.text})
        response_json = {'onvan_manzale_mozo': data}
        return response_json

    @staticmethod
    def tag_606(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'mozo': child.text})
            elif code == 'j':
                data.update({'taghsim_fari_shekl': child.text})
            elif code == 'x':
                data.update({'taghsim_fari_mozoie': child.text})
            elif code == 'y':
                data.update({'taghsim_fari_joghrafiaie': child.text})
            elif code == 'z':
                data.update({'taghsim_fari_doreie': child.text})
            elif code == '9':
                data.update({'mozo_mostanad_nashode': child.text})
        response_json = {'mozo': data}
        return response_json

    @staticmethod
    def tag_607(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'nam_joghrafiaie': child.text})
            elif code == 'j':
                data.update({'taghsim_fari_shekli': child.text})
            elif code == 'x':
                data.update({'taghsim_fari_mozoie': child.text})
            elif code == 'y':
                data.update({'taghsim_fari_joghrafiaie': child.text})
            elif code == 'z':
                data.update({'taghsim_fari_doreie': child.text})
            elif code == '9':
                data.update({'mozo_mostanad_nashode': child.text})
        response_json = {'nam_joghrafiaie': data}
        return response_json

    @staticmethod
    def tag_610(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'tosifgar': child.text})
        response_json = {'tosifgar': data}
        return response_json

    @staticmethod
    def tag_676(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'rade_asli_fari': child.text})
            elif code == 'b':
                data.update({'neshane_asar': child.text})
            elif code == '9':
                data.update({'marja': child.text})
        response_json = {'radebandi_ddc': data}
        return response_json

    @staticmethod
    def tag_680(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'rade_asli_fari': child.text})
            elif code == 'b':
                data.update({'shomare_kater': child.text})
            elif code == '9':
                data.update({'marja': child.text})
        response_json = {'radebandi_kongere': data}
        return response_json

    @staticmethod
    def tag_686(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'rade_asli_fari': child.text})
            elif code == 'b':
                data.update({'shomare_kater': child.text})
            elif code == '9':
                data.update({'marja': child.text})
        response_json = {'radebandi_nlm': data}
        return response_json

    @staticmethod
    def tag_700(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'nam_khanevadegi': child.text})
            elif code == 'b':
                data.update({'nam': child.text})
            elif code == 'f':
                data.update({'sal_tavalod_vafat': child.text})
            elif code == '4':
                data.update({'naghsh': child.text})
            elif code == '9':
                data.update({'nam_shakhs': child.text})
        response_json = {'sarshenasname': data}
        return response_json

    @staticmethod
    def tag_701(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'nam_khanevadegi': child.text})
            elif code == 'b':
                data.update({'nam': child.text})
            elif code == 'f':
                data.update({'sal_tavalod_vafat': child.text})
            elif code == '4':
                data.update({'naghsh': child.text})
            elif code == '9':
                data.update({'nam_shakhs': child.text})
        response_json = {'shenase_afzode_nevisande_hamkar': data}
        return response_json

    @staticmethod
    def tag_702(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'nam_khanevadegi': child.text})
            elif code == 'b':
                data.update({'nam': child.text})
            elif code == 'f':
                data.update({'sal_tavalod_vafat': child.text})
            elif code == '4':
                data.update({'naghsh': child.text})
            elif code == '9':
                data.update({'nam_shakhs': child.text})
        response_json = {'shenase_afzode_sayer_padidavaran': data}
        return response_json

    @staticmethod
    def tag_710(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'nam_confrans': child.text})
            elif code == 'b':
                data.update({'nam_fari_sazman': child.text})
            elif code == 'd':
                data.update({'shomare_hamayesh': child.text})
            elif code == 'e':
                data.update({'mahal_hamayesh': child.text})
            elif code == 'f':
                data.update({'tarikh_hamayesh': child.text})
            elif code == '4':
                data.update({'naghsh': child.text})
            elif code == '9':
                data.update({'nam_sazman': child.text})
        response_json = {'sarshenase_nam_tanalegan': data}
        return response_json

    @staticmethod
    def tag_711(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'nam_sazman': child.text})
            elif code == 'b':
                data.update({'nam_fari_sazman': child.text})
            elif code == '4':
                data.update({'naghsh': child.text})
            elif code == '9':
                data.update({'nam_sazman_mostanad_nashode': child.text})
        response_json = {'shenase_afzode_nevisande_hamkar_nam_tanalegan': data}
        return response_json

    @staticmethod
    def tag_712(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'nam_sazman': child.text})
            elif code == 'b':
                data.update({'nam_fari_sazman': child.text})
            elif code == '4':
                data.update({'naghsh': child.text})
            elif code == '9':
                data.update({'nam_sazman_mostanad_nashode': child.text})
        response_json = {'shenase_afzode_nam_tanalegan': data}
        return response_json

    @staticmethod
    def tag_801(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'keshvar': child.text})
            elif code == 'b':
                data.update({'sazman': child.text})
        response_json = {'mabda_asli': data}
        return response_json

    @staticmethod
    def tag_856(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'nam_file_enteghal_dade_shode': child.text})
            elif code == 'b':
                data.update({'nam_electronic': child.text})
        response_json = {'dastresi_mahal_electronic': data}
        return response_json

    @staticmethod
    def tag_910(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'nam_fehrest_nevis': child.text})
            elif code == 's':
                data.update({'chekide_nevis': child.text})
            elif code == 'b':
                data.update({'nam_panchist': child.text})
            elif code == 'c':
                data.update({'tarikh_fehrest_nevisi': child.text})
            elif code == 'd':
                data.update({'nam_khanevadegi_operator': child.text})
            elif code == 'e':
                data.update({'nam_operator': child.text})
            elif code == 'f':
                data.update({'tarikh_vorod_etelaat': child.text})
            elif code == 'r':
                data.update({'nam_bazbin_nahaie': child.text})
            elif code == 'h':
                data.update({'sarparast_fehrest_nevisi': child.text})
            elif code == 't':
                data.update({'tarikh_control': child.text})
            elif code == 'i':
                data.update({'tarikh_vorod': child.text})
        response_json = {'etelaat_fehrest_nevis_operator': data}
        return response_json

    @staticmethod
    def tag_923(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'format_enteshar': child.text})
        response_json = {'vaziat_enteshar': data}
        return response_json

    @staticmethod
    def tag_930(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'noe_made': child.text})
            elif code == 'b':
                data.update({'code_karbarge': child.text})
        response_json = {'etelaat_record_ketabshenasi': data}
        return response_json

    @staticmethod
    def tag_932(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'sath_dastresi': child.text})
            elif code == 'b':
                data.update({'takmil_shode': child.text})
        response_json = {'etelaat_dastresi_record': data}
        return response_json

    @staticmethod
    def tag_970(record):
        children = record.getchildren()
        data = {}
        for child in children:
            code = child.get('code')
            if code == 'a':
                data.update({'mondarejat': child.text})
        response_json = {'fehrest_mondarejat': data}
        return response_json

    def switcher(self, record):
        tag = record.get('tag')
        switching = {
            "010": self.tag_010,
            "020": self.tag_020,
            "100": self.tag_100,
            "101": self.tag_101,
            "200": self.tag_200,
            "205": self.tag_205,
            "210": self.tag_210,
            "215": self.tag_215,
            "225": self.tag_225,
            "300": self.tag_300,
            "304": self.tag_304,
            "305": self.tag_305,
            "306": self.tag_306,
            "307": self.tag_307,
            "308": self.tag_308,
            "312": self.tag_312,
            "320": self.tag_320,
            "327": self.tag_327,
            "330": self.tag_330,
            "500": self.tag_500,
            "510": self.tag_510,
            "512": self.tag_512,
            "516": self.tag_516,
            "517": self.tag_517,
            "600": self.tag_600,
            "601": self.tag_601,
            "605": self.tag_605,
            "606": self.tag_606,
            "607": self.tag_607,
            "610": self.tag_610,
            "676": self.tag_676,
            "680": self.tag_680,
            "686": self.tag_686,
            "700": self.tag_700,
            "701": self.tag_701,
            "702": self.tag_702,
            "710": self.tag_710,
            "711": self.tag_711,
            "712": self.tag_712,
            "801": self.tag_801,
            "856": self.tag_856,
            "910": self.tag_910,
            "923": self.tag_923,
            "930": self.tag_930,
            "932": self.tag_932,
            "970": self.tag_970,
        }
        resp = switching.get(tag, lambda a: {})
        return resp(record)


#
# isbn1 = '9789647468923'
# isbn2 = '9789644482311'
# isbn3 = '9789643779313'
# isbn4 = '9789644531125'
# isbn5 = '9786003590151'
# isbn6 = '9789643112004'
#
# obj = LibReader()
# response = obj.search_isbn(isbn4)
# ic(response[0].data)
# parser = Parser()
# data = parser.convert(response[0].data)


def get_first_queue_reserve(book_slug, update_book=False):
    try:
        book = Book.objects.get(slug=book_slug)
    except:
        raise ValidationError(u"کتاب یافت نشد")
    reserve = UserBook.objects.filter(book=book_slug, status='queue').select_related('user').last()
    if not reserve:
        return False
    reserve.status = 'pending'
    book_title = book.onvan_padidavarande.onvan_asli
    if not book_title:
        book_title = book.record_id
    reserve.save()
    if update_book:
        book.return_book(1)
    book.lend_book(1)
    devices = FCMDevice(user=reserve.user)
    devices.send_message(data={"type": "reserve", "msg": u"کاربر گرامی کتاب {} برای شما در کتاب خانه رزرو شد.".format(
        book_title)})
    return True


def parse_tarikh(text):
    if type(text) == int:
        return text
    year = ''
    for ch in text:
        if ch.isdigit():
            year += ch
    if year:
        year = int(year)
    return year