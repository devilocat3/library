# -*- coding: utf-8 -*-
from rest_framework.views import exception_handler


def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)
    data = getattr(response, 'data', None)
    if data:
        if type(data) == list:
            r_data = data[0]
        else:
            r_data = data
        response.data = ({
            "data": None,
            "status": 1,
            "message": r_data
        })
    print(data)
    return response
