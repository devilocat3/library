# -*- coding: utf-8 -*-
import jdatetime

from celery import shared_task
from .models import UserBook
from fcm_django.utils import send_notification_to_user
from notification.models import Notification


@shared_task(name='check-reserves')
def send_push_to_expired_reserves():
    reserves = UserBook.objects.filter(status='approved')
    today = jdatetime.datetime.now().date()
    for reserve in reserves:
        days_passed = today - reserve.borrow_start_date
        user_days = reserve.user.user_type.days
        if days_passed.days > user_days:
            late_days = days_passed.days - user_days
            title = u'تاخیر در بازگرداندن کتاب '
            msg = u'کابر گرامی با شماره {} شما در بازگشت کتاب {} روز تاخیر داشته‌اید.' \
                  u' لطفا هرچه سریعتر کتاب را به کتاب‌خانه بازگردانید'.format(reserve.user.username, late_days)
            send_notification_to_user(reserve.user, title, msg, "delay")
            Notification.objects.create(receiver=reserve.user, content=msg)
    return 'Done!'
