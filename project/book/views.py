# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth import get_user_model
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework_mongoengine.viewsets import ModelViewSet
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin, ListModelMixin, RetrieveModelMixin, DestroyModelMixin, \
    UpdateModelMixin
from rest_framework.decorators import action
from rest_framework import status
from rest_framework.parsers import JSONParser, FormParser, MultiPartParser
from itertools import chain

from user_management.responses import CustomResponse
from user_management.permissions import IsOwner, IsAdminOrHasPermission
from fcm_django.utils import send_notification_to_user
from notification.models import Notification
from fcm_django.models import FCMDevice
from logger.models import Log
from dashboard.jalali import convert_string_to_jdate
from .utils import LibReader, Parser, get_first_queue_reserve
from .filters import TarhFilters, MaghaleFilters, UserBookFilters, UserFavouriteFilters
from .serializers import *
from .permissions import ListOwnerPermission

User = get_user_model()


class UserBooksViewSet(GenericViewSet, CreateModelMixin, ListModelMixin, RetrieveModelMixin, DestroyModelMixin):
    model = UserBook
    serializer_class = UserBookSerializer
    queryset = UserBook.objects.all()
    permission_classes = [IsAdminOrHasPermission]
    needed_permissions = ['view_user_books', 'manage_user_books']
    filter_class = UserBookFilters

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.action == 'create':
            self.permission_classes = [IsAuthenticated]
        elif self.action == 'retrieve':
            self.permission_classes = [IsOwner]
        elif self.action == 'list':
            self.permission_classes = [ListOwnerPermission]
        return [permission() for permission in self.permission_classes]

    def get_object(self):
        queryset = self.filter_queryset(UserBook.objects.all())

        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        assert lookup_url_kwarg in self.kwargs, (
                'Expected view %s to be called with a URL keyword argument '
                'named "%s". Fix your URL conf, or set the `.lookup_field` '
                'attribute on the view correctly.' %
                (self.__class__.__name__, lookup_url_kwarg)
        )

        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        obj = get_object_or_404(queryset, **filter_kwargs)

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj

    def get_queryset(self):
        if self.request.user.is_staff:
            pending = self.model.objects.filter(status='pending').order_by('-created')
            others = self.model.objects.exclude(status='pending').order_by('-created')
            q = list(chain(pending, others))
            return q
        else:
            q = self.model.objects.filter(user=self.request.user)
            pending = q.filter(user=self.request.user, status='pending').order_by('-created')
            others = q.exclude(user=self.request.user, status='pending').order_by('-created')
            q = list(chain(pending, others))
            return q

    def create(self, request, *args, **kwargs):
        """
        creating reserve For User based on the given info. it only needs book slug
        """
        data = request.data
        data.update({"user": request.user.id})
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        reserve = serializer.save()
        headers = self.get_success_headers(serializer.data)
        content = u'درخواست امانت کتاب برای کتاب {} داده شد.'.format(serializer.data['book']['slug'])
        Log.objects.create_log(content, request.user.username, request.user.is_staff, 'create')
        return CustomResponse(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def list(self, request, *args, **kwargs):
        """
        If request user is admin it will list all reserved books else it will return
        all the books for the request user
        """
        if request.user.is_staff:
            q = UserBook.objects.all()
        else:
            q = UserBook.objects.filter(user=request.user).order_by('-created')
        queryset = self.filter_queryset(q)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            resp = self.get_paginated_response(serializer.data)
            return CustomResponse(data=resp.data)

        serializer = self.get_serializer(queryset, many=True)
        return CustomResponse(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        """
        It will show the detail of a book reserve!
        """
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return CustomResponse(serializer.data)

    @action(methods=['post'], url_path='user/books', serializer_class=UsernameSerializer, detail=False)
    def get_user_books(self, request, **kwargs):
        """
        Admin access Only, it will show the reserved books for the given user

        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        books = self.model.objects.filter(user__username=serializer.validated_data.get('username')).order_by('-created')
        page = self.paginate_queryset(books)
        if page is not None:
            serializer = UserBookSerializer(page, many=True, context={'request': request})
            resp = self.get_paginated_response(serializer.data)
            return CustomResponse(data=resp.data)

        serializer = UserBookSerializer(books, many=True, context={'request': request})
        return CustomResponse(serializer.data)

    @action(methods=['get'], url_path='user', detail=False, permission_classes=[IsAuthenticated], needed_permissions=[])
    def get_user_reserves(self, request, **kwargs):
        """
        returns the current logged in user's reserved books & reserve requests
        """
        # TODO paginate the result!
        books = UserBook.objects.filter(user=request.user).order_by('-created')
        serializer = self.get_serializer(data=books, many=True)
        serializer.is_valid()
        return CustomResponse(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['post'], url_path='response', detail=False, serializer_class=ReserveResponseSerializer,
            permission_classes=[IsAdminOrHasPermission], needed_permissions=['manage_user_books'])
    def response_user_reserve(self, request, **kwargs):
        """
        Admin can approve user request for a book, if the given status is c it will cancel the request,
        if it's a it will approve the request.
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        state = serializer.validated_data.get('status')
        user_book = self.model.objects.filter(id=serializer.validated_data.get('user_book_id')).first()
        if not user_book:
            raise ValidationError('درخواستی با این ایدی یافت نشد.')
        book = Book.objects.filter(slug=user_book.book).first()
        try:
            title = book.onvan_padidavarande.onvan_asli
            if not title:
                title = book.slug
        except:
            title = book.slug
        manager = u'کارمند'
        if request.user.is_superuser:
            manager = u'مدیر'
        if state == 'a':
            msg = u'درخواست برای رزرو کتاب {} توسط مدیریت تایید شد.'.format(title)
            if user_book.approve_request():
                send_notification_to_user(user=user_book.user, title='تایید رزرو', msg=msg, type="reserve")

            content = u'{}، درخواست کاربر {} برای کتاب {} را تایید کرد'.format(manager, user_book.user.username, title)
            log_type = 'approve'
            next_queue = False
        elif state == 'c':
            msg = u'درخواست برای رزرو کتاب {} توسط مدیریت رد شد.'.format(title)
            user_book.cancel_request()
            send_notification_to_user(user=user_book.user, title='لغو رزرو', msg=msg, type="reserve")
            content = u'{}، درخواست کاربر {} برای کتاب {} را لغو کرد'.format(manager, user_book.user.username, title)
            log_type = 'canceled'
            # Accept the next reserve in queue
            next_queue = True
        else:
            user_book.return_book()
            msg = u'کتاب {} با موفقیت به کتابخانه برگشت داده شد.'.format(title)
            send_notification_to_user(user=user_book.user, title='برگشت کتاب', msg=msg, type="reserve")


            content = u'{}، کتاب {} را برای کاربر {} برگشت داد'.format(manager, title, user_book.user.username)
            next_queue = True
            log_type = 'return'
        if next_queue:
            get_first_queue_reserve(user_book.book)
        Notification.objects.create(receiver=user_book.user, content=msg)
        Log.objects.create_log(content, request.user.username, request.user.is_staff, log_type)
        serializer = UserBookSerializer(user_book, context={'request': request})
        return CustomResponse(data=serializer.data, message='درخواست با موفقیت انجام شد.')

    @action(methods=['post'], url_path='delay/notify', detail=True, serializer_class=ReservePushSerializer)
    def notify_user_for_delay(self, request, pk, **kwargs):
        """
        Admin can send notifications to user to return the book
        :param request:
        :param pk:
        :param kwargs:
        :return:
        """
        reserve = self.get_object()
        if not reserve.status == 'approved':
            raise ValidationError(u'کتاب در دسترس کاربر نمی‌باشد.')
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        send_notification_to_user(user=reserve.user, title=serializer.validated_data.get('title'),
                                  msg=serializer.validated_data.get('message'), type="delay")
        Notification.objects.create(receiver=reserve.user, content=serializer.validated_data.get('message'))
        return CustomResponse(serializer.data)

    @action(methods=['get'], url_path='cancel', detail=True, permission_classes=[IsAuthenticated])
    def cancel_user_reserve(self, request, pk, **kwargs):
        """
        user can cancel it reserve if its in 2 states
        :param request:
        :param kwargs:
        :return:
        """
        try:
            reserve = UserBook.objects.get(id=pk)
        except:
            raise ValidationError(u'رزرو مورد نظر یافت نشد.')
        if not reserve.user == request.user:
            raise ValidationError(u'شما توانایی لغو این درخواست را ندارید')
        if reserve.status == 'pending' or reserve.status == 'queue':
            reserve.cancel_request()
            if reserve.status == 'pending':
                get_first_queue_reserve(reserve.book)
            content = u'کاربر درخواستش برای کتاب {} را لغو کرد'.format(reserve.book)
            Log.objects.create_log(content, reserve.user.username, request.user.is_staff, 'canceled')
            return CustomResponse(message=u'با موفقیت انجام شد')
        raise ValidationError(u'درخواست قابل لغو شدن نمی‌باشد.')

    @action(methods=['get'], url_path='calculate/penalty', detail=False, permission_classes=[IsAuthenticated])
    def calculate_penalty(self, request, **kwargs):
        reserves = UserBook.objects.filter(status='returned', user=request.user)
        penalty_days = 0
        for reserve in reserves:
            penalty_days += reserve.penalty
        try:
            penalty = penalty_days * request.user.user_type.penalty
        except:
            raise ValidationError(u'نوع کاربر مشخص نشده است')
        return CustomResponse({'penalty_cost': penalty, 'penalty_days': penalty_days})

    @action(methods=['post'], url_path='calculate/user/penalty', detail=False,
            permission_classes=[IsAdminOrHasPermission],
            serializer_class=UsernameSerializer)
    def calculate_penalty_admin(self, request, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        username = serializer.validated_data['username']
        try:
            user = User.objects.get(username=username)
        except:
            raise ValidationError(u'کاربری با این نام‌کاربری یافت نشد.')
        reserves = UserBook.objects.filter(status='returned', user=user)
        penalty_days = 0
        for reserve in reserves:
            penalty_days += reserve.penalty
        penalty = penalty_days * user.user_type.penalty
        data = {
            'penalty_days': penalty_days,
            'penalty_cost': penalty,
            'user': {
                'username': user.username,
                'id': user.id,
            }
        }
        return CustomResponse(data)

    @action(methods=['post'], detail=False, url_path='checkout', serializer_class=UsernameSerializer,
            permission_classes=[IsAdminOrHasPermission])
    def checkout_user(self, request, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        username = serializer.validated_data['username']
        try:
            user = User.objects.get(username=username)
        except:
            raise ValidationError(u'کاربری با این نام‌کاربری یافت نشد.')
        reserves = UserBook.objects.filter(status='returned', user=user)
        penalty_days = 0
        for reserve in reserves:
            penalty_days += reserve.penalty
            reserve.penalty = 0
            reserve.save()
        data = {
            'before_checkout_penalty_days': penalty_days,
            'after_checkout': 0,
            'user': {
                'username': user.username,
                'id': user.id
            }
        }
        return CustomResponse(data)


class BookViewSet(ModelViewSet):
    queryset = Book.objects.all()
    model = Book
    serializer_class = BookSerializer
    lookup_field = 'slug'
    permission_classes = []
    needed_permissions = []

    def get_permissions(self):
        if self.action == 'destroy' or self.action == 'create':
            self.permission_classes = [IsAdminOrHasPermission]
            self.needed_permissions = ['manage_books']
        elif self.action == 'update' or self.action == 'partial_update':
            self.permission_classes = [IsAdminOrHasPermission]
            self.needed_permissions = ['manage_books']
        return [permission() for permission in self.permission_classes]

    def get_queryset(self):
        queryset = self.model.objects.all().order_by('-created')
        return queryset

    def retrieve(self, request, *args, **kwargs):
        """
        returns the detailed information about a given book
        """
        response = super(BookViewSet, self).retrieve(request, *args, **kwargs)
        return CustomResponse(response.data)

    def destroy(self, request, *args, **kwargs):
        """
        removes a book from database
        """
        instance = self.get_object()
        UserFavourite.objects.filter(slug=instance.slug).delete()
        self.perform_destroy(instance)
        manager = u'کارمند'
        if request.user.is_superuser:
            manager = u'مدیر'
        content = u'{}،کتاب {} را حذف کرد'.format(manager, instance.slug)
        Log.objects.create_log(content, request.user.username, request.user.is_staff, 'delete')
        return CustomResponse(message='با موفقیت کتاب حذف شد.', status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        """
        create a book based on given data
        """
        data = request.data
        # try:
        tarikh = data.get('nasher_specs').get('tarikh')
        data['nasher_specs']['tarikh'] = parse_tarikh(tarikh)
        # except Exception, e:
        #     print e
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        book = serializer.save()
        headers = self.get_success_headers(serializer.data)
        manager = u'کارمند'
        if request.user.is_superuser:
            manager = u'مدیر'
        content = u'{} ،کتاب {} را اضافه کرد'.format(manager, book.slug)
        Log.objects.create_log(content, request.user.username, request.user.is_staff, 'create')
        return CustomResponse(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def list(self, request, *args, **kwargs):
        """
        returns the list of books
        """
        queryset = self.filter_queryset(self.get_queryset())
        date_from = request.GET.get('date_from')
        date_to = request.GET.get('date_to')
        dfrom = request.GET.get('dfrom')
        dto = request.GET.get('dto')
        try:
            if date_from:
                date_from = convert_string_to_jdate(date_from).togregorian()
                queryset = queryset.filter(created__gte=date_from)
            if date_to:
                date_to = convert_string_to_jdate(date_to).togregorian()
                queryset = queryset.filter(created__lte=date_to)
            if dfrom:
                queryset = queryset.filter(nasher_specs__tarikh__gte=dfrom)
            if dto:
                queryset = queryset.filter(nasher_specs__tarikh__lte=dto)
        except:
            raise ValidationError(u'فیلتر نامعتبر')
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = BookListSerializer(page, many=True, context={'request': request})
            response = self.get_paginated_response(serializer.data)
            return CustomResponse(response.data)

        serializer = BookListSerializer(queryset, many=True, context={'request': request})
        return CustomResponse(serializer.data)

    # def partial_update(self, request, *args, **kwargs):
    #     """
    #     Updates a book partially, to update a book just send the changed data.
    #     :param request:
    #     :param args:
    #     :param kwargs:
    #     :return:
    #     """
    #     kwargs['partial'] = True
    #     return self.update(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        """
        update a book based on given info, it needs all the book data to update it.
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        manager = u'کارمند'
        if request.user.is_superuser:
            manager = u'مدیر'
        content = u'{} ،کتاب {} را تغییر داد'.format(manager, instance.slug)
        Log.objects.create_log(content, request.user.username, request.user.is_staff, 'change')

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return CustomResponse(serializer.data)

    @action(methods=['post'], url_path='search', detail=False)
    def search_books(self, request, **kwargs):
        """
        performs a search on the database based on given query and returns the result
        """
        date_from = request.data.get('date_from')
        date_to = request.data.get('date_to')
        dfrom = request.data.get('dfrom')
        dto = request.data.get('dto')
        queryset = self.model.objects.all()
        try:
            if date_from:
                date_from = request.data.pop('date_from')
                queryset = queryset.filter(created__gte=date_from)
            if date_to:
                date_to = request.data.pop('date_to')
                queryset = queryset.filter(created_lte=date_to)
            if dfrom:
                dfrom = request.data.pop('dfrom')
                queryset = queryset.filter(nasher_specs__tarikh__gte=dfrom)
            if dto:
                request.data.pop('dto')
                queryset = queryset.filter(nasher_specs__tarikh__lte=dto)
        except:
            raise ValidationError(u'فیلتر نامعتبر')

        try:
            queryset = queryset.filter(**self.request.data)
        except:
            raise ValidationError('سرچ زده شده معتبر نمی‌باشد.')
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = BookListSerializer(page, many=True, context={'request': request})
            return CustomResponse(self.get_paginated_response(serializer.data).data)
        serializer = BookListSerializer(queryset, many=True, context={'request': request})
        return CustomResponse(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['post'], serializer_class=IsbnSearchSerializer, url_path='search/isbn', detail=False,
            permission_classes=[IsAdminOrHasPermission], needed_permissions=['view_books', 'manage_books'])
    def search_isbn(self, request, **kwargs):
        """
        Searches the given isbn on the national library database and returns the info
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        lib_connection = LibReader()
        data = lib_connection.search_isbn(serializer.validated_data.get('isbn'))
        parsed_data = Parser().convert(data=data)
        return CustomResponse(parsed_data)

    @action(methods=['post'], serializer_class=BookInventorySerializer, url_path='inventory/add', detail=True,
            permission_classes=[IsAdminOrHasPermission], needed_permissions=['manage_books'])
    def add_book_inventory(self, request, slug, **kwargs):
        """
        adds a book to its inventory!
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        book = self.get_object()
        book.book_inventory.total = serializer.validated_data.get('total')
        book.book_inventory.available = serializer.validated_data.get('available')
        book.save()
        return CustomResponse(message="درخواست با موفقیت انجام شد.", status=status.HTTP_200_OK)

    @action(methods=['post'], serializer_class=BookReturnLendSerializer, url_path='inventory/return', detail=True,
            permission_classes=[IsAdminOrHasPermission], needed_permissions=['manage_books'])
    def return_book(self, request, slug, **kwargs):
        """
        returns a book to the database!
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        book = self.get_object()
        book.return_book(**serializer.validated_data)
        return CustomResponse(message="درخواست با موفقیت انجام شد.",
                              status=status.HTTP_200_OK)

    @action(methods=['post'], serializer_class=BookReturnLendSerializer, url_path='inventory/lend', detail=True,
            permission_classes=[IsAdminOrHasPermission], needed_permissions=['manage_books'])
    def lend_book(self, request, slug, **kwargs):
        """
        put book on loan
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        book = self.get_object()
        book.lend_book(**serializer.validated_data)
        return CustomResponse(message="درخواست با موفقیت انجام شد.", status=status.HTTP_200_OK)


class MaghaleViewSet(GenericViewSet, CreateModelMixin, ListModelMixin, DestroyModelMixin, UpdateModelMixin):
    model = Maghale
    queryset = Maghale.objects.all()
    serializer_class = MaghaleSerializer
    parser_classes = [MultiPartParser, JSONParser, FormParser]
    filter_class = MaghaleFilters
    permission_classes = []
    needed_permission = []

    def get_permissions(self):
        if self.action == 'destroy' or self.action == 'create':
            self.permission_classes = [IsAdminOrHasPermission]
            self.needed_permissions = ['manage_maghale']
        elif self.action == 'update' or self.action == 'partial_update':
            self.permission_classes = [IsAdminOrHasPermission]
            self.needed_permissions = ['manage_maghale']
        return [permission() for permission in self.permission_classes]

    def get_queryset(self):
        q = self.model.objects.all()
        return q

    def retrieve(self, request, *args, **kwargs):
        """
        returns detials about a Article
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return CustomResponse(serializer.data)

    def create(self, request, *args, **kwargs):
        """
            Use this api to add new maghale to the table!
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        maghale = serializer.save()
        headers = self.get_success_headers(serializer.data)
        manager = u'کارمند'
        if request.user.is_superuser:
            manager = u'مدیر'
        content = u'{} ،مقاله {} را اضافه کرد'.format(manager, maghale.onvan)
        Log.objects.create_log(content, request.user.username, request.user.is_staff, 'create')
        return CustomResponse(data=serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def list(self, request, *args, **kwargs):
        """
        shows the list of maghale in response
        """
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            resp = self.get_paginated_response(serializer.data)
            return CustomResponse(resp.data)

        serializer = self.get_serializer(queryset, many=True)
        return CustomResponse(serializer.data)

    def update(self, request, *args, **kwargs):
        """
        use this api to update a current maghale's info
        """
        response = super(MaghaleViewSet, self).update(request, *args, **kwargs)
        manager = u'کارمند'
        if request.user.is_superuser:
            manager = u'مدیر'
        content = u'{} ،مقاله {} را تغییر داد'.format(manager, response.data['onvan'])
        Log.objects.create_log(content, request.user.username, request.user.is_staff, 'change')
        return CustomResponse(response.data)

    def destroy(self, request, *args, **kwargs):
        """
        use this api to remove maghale
        """
        instance = self.get_object()
        UserFavourite.objects.filter(slug=instance.id).delete()
        manager = u'کارمند'
        if request.user.is_superuser:
            manager = u'مدیر'
        content = u'{} ،مقاله {} را حذف کرد'.format(manager, instance.onvan)
        instance.delete()
        Log.objects.create_log(content, request.user.username, request.user.is_staff, 'delete')
        return CustomResponse()


class TarhViewSet(GenericViewSet, CreateModelMixin, ListModelMixin, DestroyModelMixin, UpdateModelMixin):
    queryset = Tarh.objects.all()
    serializer_class = TarhSerializer
    permission_classes = []
    needed_permission = []
    filter_class = TarhFilters

    def get_permissions(self):
        if self.action == 'destroy' or self.action == 'create':
            self.permission_classes = [IsAdminOrHasPermission]
            self.needed_permissions = ['manage_tarh']
        elif self.action == 'update' or self.action == 'partial_update':
            self.permission_classes = [IsAdminOrHasPermission]
            self.needed_permissions = ['manage_tarh']
        return [permission() for permission in self.permission_classes]

    def retrieve(self, request, *args, **kwargs):
        """
        returns detials about a Plan
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return CustomResponse(serializer.data)

    def create(self, request, *args, **kwargs):
        """
            Use this api to add new tarh to the table!
        """
        response = super(TarhViewSet, self).create(request, *args, **kwargs)
        manager = u'کارمند'
        if request.user.is_superuser:
            manager = u'مدیر'
        content = u'{} ،طرح {} را اضافه کرد'.format(manager, response.data.get('onvan', '??'))
        Log.objects.create_log(content, request.user.username, request.user.is_staff, 'create')
        return CustomResponse(response.data)

    def list(self, request, *args, **kwargs):
        """
        shows the list of tarh in response
        """
        response = super(TarhViewSet, self).list(request, *args, **kwargs)
        return CustomResponse(response.data)

    def update(self, request, *args, **kwargs):
        """
        use this api to update a current tarh's info
        """
        response = super(TarhViewSet, self).update(request, *args, **kwargs)
        manager = u'کارمند'
        if request.user.is_superuser:
            manager = u'مدیر'
        content = u'{} ،طرح {} را تغییر داد'.format(manager, response.data.get('onvan'), '??')
        Log.objects.create_log(content, request.user.username, request.user.is_staff, 'change')
        return CustomResponse(response.data)

    def destroy(self, request, *args, **kwargs):
        """
        use this api to remove tarh
        """
        manager = u'کارمند'
        if request.user.is_superuser:
            manager = u'مدیر'
        instance = self.get_object()
        content = u'{} ،طرح {} را حذف کرد'.format(manager, instance.onvan)
        Log.objects.create_log(content, request.user.username, request.user.is_staff, 'delete')
        UserFavourite.objects.filter(slug=instance.id).delete()
        instance.delete()
        return CustomResponse(message='با موفقیت حذف شد')


class FavouriteViewSet(GenericViewSet):
    queryset = UserFavourite.objects.all()
    serializer_class = FavouriteSerializer
    filter_class = UserFavouriteFilters
    permission_classes = [IsAuthenticated]
    needed_permissions = []

    @action(methods=['get'], url_path='list', detail=False)
    def show_favourite(self, request):
        favourites = request.user.favourite.all()
        queryset = self.filter_queryset(favourites)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            resp = self.get_paginated_response(serializer.data)
            return CustomResponse(resp.data)

        serializer = self.get_serializer(queryset, many=True)
        return CustomResponse(serializer.data)

    @action(methods=['post'], url_path='add', detail=False)
    def add_to_favourite(self, request):
        data = request.data
        data.update({'user': request.user.id})
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return CustomResponse(data=serializer.data)

    @action(methods=['delete'], url_path='favourite/remove', detail=True, permission_classes=[IsOwner])
    def remove_from_favourite(self, request, **kwargs):
        fav_object = self.get_object()
        fav_object.delete()
        return CustomResponse(message='با موفقیت حذف شد')
