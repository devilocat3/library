# -*- coding: utf-8 -*-
import jdatetime
from django_filters import rest_framework as filters

from dashboard.jalali import convert_string_to_jdate
from .models import Book, Maghale, Tarh, UserBook, UserFavourite

TYPES = (('book', 'کتاب'),
         ('maghale', 'مقاله'),
         ('tarh', 'طرح'))
REQUEST_STATUS = (
    ('canceled', 'لغو شده'),
    ('approved', 'تایید شده'),
    ('pending', 'در انتظار تایید'),
    ('queue', 'در صف انتظار'),
    ('returned', 'تحویل داده شده'),
)


class MaghaleFilters(filters.FilterSet):
    author = filters.CharFilter(name="nevisande", lookup_expr='icontains')
    title = filters.CharFilter(name='onvan', lookup_expr='icontains')
    subject = filters.CharFilter(name='mozo', lookup_expr='icontains')
    date = filters.CharFilter(method='date_filter')
    magazine = filters.CharFilter(name="majale", lookup_expr='icontains')
    date_from = filters.CharFilter(method='custom_date_filter', name='created__gte')
    date_to = filters.CharFilter(method='custom_date_filter', name="created__lte")
    dfrom = filters.CharFilter(method='custom_date_filter', name="tarikh__gte")
    dto = filters.CharFilter(method='custom_date_filter', name="tarikh__lte")
    class Meta:
        model = Maghale
        fields = ['author', 'title', 'date', 'subject', 'date_to', 'date_from', 'magazine']

    def date_filter(self, qs, name, value):
        try:
            value = int(value)
            jtime_start = jdatetime.date(int(value), 01, 01)
            jtime_end = jdatetime.date(int(value), 12, 29)
        except:
            return qs
        return qs.filter(tarikh__gte=jtime_start).filter(tarikh__lte=jtime_end)

    def custom_date_filter(self, qs, name, value):
        jtime = convert_string_to_jdate(value)
        qs = qs.filter(**{name: jtime})
        return qs


class TarhFilters(filters.FilterSet):
    author = filters.CharFilter(name="nevisande", lookup_expr='icontains')
    title = filters.CharFilter(name='onvan', lookup_expr='icontains')
    subject = filters.CharFilter(name='mozo', lookup_expr='icontains')
    date = filters.CharFilter(method='date_filter')
    date_from = filters.CharFilter(method='custom_date_filter', name='created__gte')
    date_to = filters.CharFilter(method='custom_date_filter', name="created__lte")
    dfrom = filters.CharFilter(method='custom_date_filter', name="tarikh__gte")
    dto = filters.CharFilter(method='custom_date_filter', name="tarikh__lte")
    class Meta:
        model = Tarh
        fields = ['author', 'title', 'date', 'subject', 'date_to', 'date_from']

    def date_filter(self, qs, name, value):
        try:
            value = int(value)
            jtime_start = jdatetime.date(int(value), 01, 01)
            jtime_end = jdatetime.date(int(value), 12, 29)
        except:
            return qs
        return qs.filter(tarikh__gte=jtime_start).filter(tarikh__lte=jtime_end)

    def custom_date_filter(self, qs, name, value):
        jtime = convert_string_to_jdate(value)
        qs = qs.filter(**{name: jtime})
        return qs


class UserBookFilters(filters.FilterSet):
    book = filters.CharFilter(name="book", lookup_expr='exact')
    user = filters.CharFilter(name="user__username", lookup_expr='exact')
    active = filters.BooleanFilter(name="active")
    bsd_to = filters.CharFilter(name="borrow_start_date__lte", method='custom_borrow_date_filter')
    bsd_from = filters.CharFilter(name="borrow_start_date__gte", method='custom_borrow_date_filter')
    brd_to = filters.CharFilter(name="borrow_return_date__lte", method='custom_borrow_date_filter')
    brd_from = filters.CharFilter(name="borrow_return_date__gte", method='custom_borrow_date_filter')
    status = filters.ChoiceFilter(name='status', lookup_expr='exact', choices=REQUEST_STATUS)
    date_from = filters.CharFilter(method='custom_date_filter', name='created__gte')
    date_to = filters.CharFilter(method='custom_date_filter', name="created__lte")
    title = filters.CharFilter(field_name="book", method="get_title")

    class Meta:
        model = UserBook
        fields = ['bsd_to','brd_to', 'brd_from', 'bsd_from', 'user', 'book', 'active', 'date_to', 'date_from']

    def get_title(self, qs, name, value):
        book = Book.objects.filter(onvan_padidavarande__onvan_asli__icontains=value).first()
        if not book:
            return qs
        return qs.filter(book=book.slug)

    def custom_date_filter(self, qs, name, value):
        jtime = convert_string_to_jdate(value)
        qs = qs.filter(**{name: jtime})
        return qs
    def custom_borrow_date_filter(self, qs, name, value):
        jtime = convert_string_to_jdate(value).date()
        qs = qs.filter(**{name: jtime})
        return qs


class UserFavouriteFilters(filters.FilterSet):
    type = filters.ChoiceFilter(name='type', lookup_expr='exact', choices=TYPES)

    class Meta:
        model = UserFavourite
        fields = ['type']
