# -*- coding: utf-8 -*-
from django.db.models.signals import post_save
from django.contrib.auth import get_user_model
from fcm_django.models import FCMDevice
from .models import UserBook, Book

User = get_user_model()


def send_notificaton_admin(sender, **kwargs):
    reserve = kwargs["instance"]
    if kwargs["created"]:
        if reserve.status == 'pending':
            book = Book.objects.filter(slug=reserve.book).first()
            msg = u"کاربر %s درخواست رزرو کتاب %s را داده است." % (
                reserve.user.get_full_name(), book.onvan_padidavarande.onvan_asli)
            data = {"full_name": reserve.user.get_full_name(), "book": book.onvan_padidavarande.onvan_asli,
                    "reserve_id": reserve.id, "book_slug": reserve.book, "type": "reserve"}
            users = User.objects.filter(is_superuser=True)
            staffs = User.objects.filter(
                user_role__role__permissions__name__in=["view_user_books", "manage_user_books"])

            devices = FCMDevice.objects.filter(user__in=users)
            devices.send_message(title=u"درخواست رزرو", body=msg, data=data)
            devices = FCMDevice.objects.filter(user__in=staffs)
            devices.send_message(title=u"درخواست رزرو", body=msg, data=data)


post_save.connect(send_notificaton_admin, UserBook)
