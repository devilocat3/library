from rest_framework.permissions import BasePermission


class ListOwnerPermission(BasePermission):
    def has_permission(self, request, view):
        if not request.user.is_authenticated:
            return False
        elif request.user.is_superuser:
            return True
        if request.user.is_staff:
            role = getattr(request.user, 'user_role', None)
            if role:
                perms = role.get_perms()
                needed_permissions = view.needed_permissions
                has_perm = perms.filter(name__in=needed_permissions)
                if has_perm:
                    return True
                else:
                    return False
            return False
        return True
