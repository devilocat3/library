# -*- coding: utf-8 -*-
from rest_framework_mongoengine.routers import DefaultRouter
from rest_framework.routers import DefaultRouter as MainRouter
from django.conf.urls import url, include
from .views import BookViewSet, UserBooksViewSet, MaghaleViewSet, TarhViewSet, FavouriteViewSet

sql_router = MainRouter()
router = DefaultRouter()
router.register('^book', BookViewSet)
sql_router.register('^reserve', UserBooksViewSet)
sql_router.register('^maghale', MaghaleViewSet)
sql_router.register('^tarh', TarhViewSet)
sql_router.register('^favourites', FavouriteViewSet)

urlpatterns = [
    url('', include(router.urls)),
    url('', include(sql_router.urls))
]