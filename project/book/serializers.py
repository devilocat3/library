# -*- coding: utf-8 -*-
import jdatetime
from dateutil import parser
from django.db.models import Q
from rest_framework.exceptions import ValidationError
from rest_framework_mongoengine import serializers
from rest_framework import serializers as main_serializers

from book.utils import get_first_queue_reserve, parse_tarikh
from .models import Book, BookInventory, UserBook, Maghale, Tarh, UserFavourite


class BookInventorySerializer(serializers.EmbeddedDocumentSerializer):
    class Meta:
        model = BookInventory
        fields = '__all__'
        read_only_fields = ('id',)


class BookReturnLendSerializer(main_serializers.Serializer):
    # slug = main_serializers.CharField(max_length=40)
    val = main_serializers.IntegerField(min_value=0, required=False)


class BookSerializer(serializers.DocumentSerializer):
    availability = main_serializers.SerializerMethodField()
    is_favourite = main_serializers.SerializerMethodField()
    is_reserved_by_user = main_serializers.SerializerMethodField('user_reserve')
    created = main_serializers.SerializerMethodField()

    class Meta:
        model = Book
        fields = '__all__'
        read_only_fields = ('id', 'slug', 'created')
        depth = 1

    def get_availability(self, obj):
        if obj.reservable:
            if obj.book_inventory.available > 0:
                return 'o'  # the book can be reserved, it's available in the library
            return 'r'  # the book can be reserved, but it's not available in the library right now
        return 'n'  # the book can't be reserved.

    def get_is_favourite(self, obj):
        data = {
            "is_favourite": False,
            "id": None
        }

        user = self.context['request'].user
        if not user.is_authenticated:
            return data
        favourite = UserFavourite.objects.filter(user=user, slug=obj.slug, type='book').first()
        if favourite:
            return data.update({
                "is_favourite": True,
                "id": favourite.id
            })
        return data

    def user_reserve(self, obj):
        user = self.context['request'].user
        if not user.is_authenticated:
            return {}
        reserve = UserBook.objects.filter(
            Q(user=user, book=obj.slug, status='pending') | Q(user=user, book=obj.slug, status='queue') | Q(user=user,
                                                                                                            book=obj.slug,
                                                                                                            status='approved')).first()
        if reserve:
            data = {'status': reserve.status,
                    'id': reserve.id
                    }
            return data
        return {}

    def get_created(self, obj):
        try:
            jtime = jdatetime.datetime.fromgregorian(datetime=obj.created)
        except ValueError:
            jtime = jdatetime.datetime.fromgregorian(datetime=parser.parse(obj.created))
        return jtime.strftime("%Y-%m-%d %H:%M:%S")

    def update(self, instance, validated_data):
        available_now = validated_data.get('book_inventory').get("available")

        available_before = instance.book_inventory.available
        queue_reqs_changed = 0
        if available_now > available_before:
            count = available_now - available_before
            for item in xrange(count):
                is_any_queue = get_first_queue_reserve(instance.slug, update_book=True)
                print is_any_queue
                if not is_any_queue:
                    break
                queue_reqs_changed +=1
        available_now -= queue_reqs_changed
        validated_data['book_inventory']['available'] = available_now
        return super(BookSerializer, self).update(instance, validated_data)

class BookListSerializer(serializers.DocumentSerializer):
    availability = main_serializers.SerializerMethodField()
    type = main_serializers.SerializerMethodField()
    is_favourite = main_serializers.SerializerMethodField()
    is_reserved_by_user = main_serializers.SerializerMethodField('user_reserve')
    created = main_serializers.SerializerMethodField()

    class Meta:
        model = Book
        fields = ['nasher_specs', 'record_id', 'onvan_padidavarande', 'radebandi_ddc', 'radebandi_kongere', 'mozo',
                  'slug', 'shabak', 'isbn', 'availability', 'book_inventory', 'type', 'is_favourite', 'created',
                  'is_reserved_by_user']
        depth = 1

    def get_availability(self, obj):
        if obj.reservable:
            if obj.book_inventory.available > 0:
                return 'o'  # the book can be reserved, it's available in the library
            return 'r'  # the book can be reserved, but it's not available in the library right now
        return 'n'  # the book can't be reserved.

    def get_type(self, obj):
        return u'کتاب'

    def user_reserve(self, obj):
        user = self.context['request'].user
        if not user.is_authenticated:
            return {}
        reserve = UserBook.objects.filter(
            Q(user=user, book=obj.slug, status='pending') | Q(user=user, book=obj.slug, status='queue') | Q(user=user,
                                                                                                            book=obj.slug,
                                                                                                            status='approved')).first()
        if reserve:
            data = {'status': reserve.status,
                    'id': reserve.id
                    }
            return data
        return {}

    def get_is_favourite(self, obj):
        data = {
            "is_favourite": False,
            "id": None
        }

        user = self.context['request'].user
        if not user.is_authenticated:
            return data
        favourite = UserFavourite.objects.filter(user=user, slug=obj.slug, type='book').first()
        if favourite:
            data.update({
                "is_favourite": True,
                "id": favourite.id
            })
            return data
        return data

    def get_created(self, obj):
        try:
            jtime = jdatetime.datetime.fromgregorian(datetime=obj.created)
        except ValueError:
            jtime = jdatetime.datetime.fromgregorian(datetime=parser.parse(obj.created))
        return jtime.strftime("%Y-%m-%d %H:%M:%S")


class IsbnSearchSerializer(main_serializers.Serializer):
    isbn = main_serializers.RegexField(regex=r'^\d{10}(\d{3})?$', required=True)


class UsernameSerializer(main_serializers.Serializer):
    username = main_serializers.CharField(required=True)


class UserBookSerializer(main_serializers.ModelSerializer):
    created = main_serializers.SerializerMethodField()
    class Meta:
        model = UserBook
        fields = '__all__'
        read_only_fields = ['id', 'penalty', 'borrow_return_date', 'renewed', 'active']


    def get_created(self, obj):
        return obj.created.strftime("%Y-%m-%d %H:%M:%S")

    def validate(self, attrs):
        user = attrs.get('user')
        user_books_count = UserBook.objects.filter(user=user).filter(Q(status='approved')| Q(status='pending')| Q(status='queue')).count()
        try:
            user_limit = user.user_type.number_of_books
        except:
            raise ValidationError(u'خطا در محاسبه ظرفیت کاربر')
        if user_limit <= user_books_count:
            raise ValidationError(u'شما به حداکثر ظرفیت رزرو خود رسیده اید.')

        book = Book.objects.filter(slug=attrs.get('book')).first()
        if not book:
            raise ValidationError('کتاب یافت نشد.')
        books = UserBook.objects.filter(
            Q(user=attrs.get('user'), book=book.slug, status='approved') | Q(user=attrs.get('user'), book=book.slug,
                                                                             status='pending') | Q(
                user=attrs.get('user'), book=book.slug, status='queue'))
        if books:
            raise ValidationError("قبلا برای این کتاب درخواست داده اید.")
        availability = book.check_book_availability()
        if availability:
            if availability == 'o':
                attrs.update({'status': 'pending'})
                book.lend_book(1)
            else:
                attrs.update({'status': 'queue'})
        else:
            raise ValidationError('کتاب قابل رزرو نیست.')
        return super(UserBookSerializer, self).validate(attrs)

    def to_representation(self, instance):
        context = super(UserBookSerializer, self).to_representation(instance)
        book = Book.objects.filter(slug=context.get('book')).first()
        if instance.penalty == 0:
            context.update({'penalty': None})
        if instance.user.avatar:
            avatar = self.context['request'].build_absolute_uri(instance.user.avatar.url)
        else:
            avatar = None
        if book:
            context.update({
                "user": {
                    "username": instance.user.username,
                    "id": instance.user.id,
                    "avatar": avatar,
                    "first_name": instance.user.first_name,
                    "last_name": instance.user.last_name,
                },
                "book": {
                    "isbn": book.isbn,
                    "slug": book.slug,
                    "onvan": book.onvan_padidavarande.onvan_asli,
                }
            })
        else:
            context.update({
                "user": {
                    "username": instance.user.username,
                    "id": instance.user.id,
                    "avatar": avatar,
                    "first_name": instance.user.first_name,
                    "last_name": instance.user.last_name,
                },
                "book": {
                    "isbn": "",
                    "slug": "",
                    "onvan": "",
                }
            })

        return context


class ReserveResponseSerializer(main_serializers.Serializer):
    CHOICES = ['a', 'c', 'r']
    status = main_serializers.ChoiceField(choices=CHOICES)
    user_book_id = main_serializers.IntegerField()


class MaghaleSerializer(main_serializers.ModelSerializer):
    type = main_serializers.SerializerMethodField()
    is_favourite = main_serializers.SerializerMethodField()

    class Meta:
        model = Maghale
        fields = '__all__'
        read_only_fields = ['created']

    def get_is_favourite(self, obj):
        data = {
            "is_favourite": False,
            "id": None
        }

        user = self.context['request'].user
        if not user.is_authenticated:
            return data
        favourite = UserFavourite.objects.filter(user=user, slug=obj.id, type='maghale').first()
        if favourite:
            data.update({
                "is_favourite": True,
                "id": favourite.id
            })
        return data

    def get_type(self, obj):
        return u'مقاله'


class TarhSerializer(main_serializers.ModelSerializer):
    type = main_serializers.SerializerMethodField()
    is_favourite = main_serializers.SerializerMethodField()

    class Meta:
        model = Tarh
        fields = '__all__'
        read_only_fields = ['created']

    def get_is_favourite(self, obj):
        data = {
            "is_favourite": False,
            "id": None
        }

        user = self.context['request'].user
        if not user.is_authenticated:
            return data
        favourite = UserFavourite.objects.filter(user=user, slug=obj.id, type='tarh').first()
        if favourite:
            data.update({
                "is_favourite": True,
                "id": favourite.id
            })
        return data

    def get_type(self, obj):
        return u'طرح'


class FavouriteSerializer(main_serializers.ModelSerializer):
    is_reserved = main_serializers.SerializerMethodField()
    availability = main_serializers.SerializerMethodField()

    class Meta:
        model = UserFavourite
        fields = ['user', 'slug', 'type', 'id', 'is_reserved', 'availability']
        read_only_fields = ['id']

    def get_is_reserved(self, obj):
        user = self.context['request'].user
        if not user.is_authenticated:
            return ''
        if obj.type == 'book':

            reserve = UserBook.objects.filter(
                Q(user=user, book=obj.slug, status='pending') | Q(user=user, book=obj.slug, status='queue') | Q(
                    user=user,
                    book=obj.slug,
                    status='approved')).first()
            if reserve:
                return reserve.status
        return ''

    def get_availability(self, obj):
        if not obj.type == 'book':
            return ''
        book = Book.objects.filter(slug=obj.slug).first()
        if not book:
            return ''

        if book.reservable:
            if book.book_inventory.available > 0:
                return 'o'  # the book can be reserved, it's available in the library
            return 'r'  # the book can be reserved, but it's not available in the library right now
        return 'n'  # the book can't be reserved.

    def to_representation(self, instance):
        favourite_type = instance.type

        if favourite_type == 'book':
            favourite_obj = Book.objects.filter(slug=instance.slug).first()
            if not favourite_obj:
                raise ValidationError(u'کتاب مورد نظر وجود ندارد')
            data = {
                'type': 'book',
                'onvan': favourite_obj.onvan_padidavarande.onvan_asli,
                'nevisande': favourite_obj.onvan_padidavarande.nam_nevisande,
                'tarikh': favourite_obj.nasher_specs.tarikh,
                'slug': favourite_obj.slug,
                'file': None
            }

        elif favourite_type == 'maghale':
            favourite_obj = Maghale.objects.filter(id=instance.slug).first()
            if not favourite_obj:
                raise ValidationError(u' مقاله مورد نظر وجود ندارد')
            data = {
                'type': 'maghale',
                'onvan': favourite_obj.onvan,
                'nevisande': favourite_obj.nevisande,
                'tarikh': str(favourite_obj.tarikh),
                'slug': favourite_obj.id,
                'file': None
            }
            if favourite_obj.file:
                data.update({
                    'file': self.context['request'].build_absolute_uri(favourite_obj.file.url)
                })

        else:
            favourite_obj = Tarh.objects.filter(id=instance.slug).first()
            if not favourite_obj:
                raise ValidationError(u'طرح مورد نظر وجود ندارد')
            data = {
                'type': 'tarh',
                'onvan': favourite_obj.onvan,
                'nevisande': favourite_obj.nevisande,
                'tarikh': str(favourite_obj.tarikh),
                'slug': favourite_obj.id,
                'file': None
            }
        data.update({
            'id': instance.id,
            'availability': self.get_availability(instance),
            'is_reserved': self.get_is_reserved(instance),
            'user': {
                'username': instance.user.username,
                'id': instance.user.id,
            }
        })
        return data

    def validate(self, attrs):
        slug = attrs.get('slug')
        type = attrs.get('type')
        if type == 'book':
            book = Book.objects.filter(slug=slug).first()
            if not book:
                raise ValidationError(u'کتاب مورد نظر یافت نشد')
        elif type == 'maghale':
            maghale = Maghale.objects.filter(id=slug).first()
            if not maghale:
                raise ValidationError(u'مقاله یافت نشد.')
        elif type == 'tarh':
            tarh = Tarh.objects.filter(id=slug).first()
            if not tarh:
                raise ValidationError(u'طرح یافت نشد')
        return attrs


class ReservePushSerializer(main_serializers.Serializer):
    message = main_serializers.CharField(required=True)
    title = main_serializers.CharField(required=True)
