# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import re
from datetime import datetime
from mongoengine import Document, EmbeddedDocument, fields
from django.utils.text import slugify
from random import randint
from rest_framework.exceptions import ValidationError
from django.db import models
from django_jalali.db import models as jmodels
from django.contrib.auth import get_user_model
import jdatetime

User = get_user_model()


def maghale_images_upload_address(instance, filename):
    upload_dir = "articles/%s" % filename
    return upload_dir


def book_covers_upload(instance, filename):
    upload_dir = "books/covers/%s" % filename
    return upload_dir


def generate_tracking_code():
    track_code = randint(100000000, 999999999)
    obj = UserBook.objects.filter(tracking_code=track_code).first()
    if obj:
        return generate_tracking_code()
    else:
        return track_code


def generate_slug(instance, new=False):
    id = instance.record_id
    if not id:
        id = randint(1, 10000)
    if new:
        id = '{}-{}'.format(id, randint(100, 999))
    if Book.objects.filter(record_id=id).first():
        return generate_slug(instance, new=True)
    return str(id)


class Publisher(EmbeddedDocument):
    makan = fields.StringField(max_length=30, null=True)
    esme_nasher = fields.StringField(max_length=500, null=True)
    tarikh = fields.IntField(min_value=0, null=True, )

    # todo Make Tarikh Integer field


class Mozo(EmbeddedDocument):
    mozo = fields.StringField(max_length=500, null=True)
    taghsim_fari_shekl = fields.StringField(max_length=500, null=True)
    taghsim_fari_mozoie = fields.StringField(max_length=500, null=True)
    taghsim_fari_joghrafiaie = fields.StringField(max_length=500, null=True)
    taghsim_fari_doreie = fields.StringField(max_length=500, null=True)
    mozo_mostanad_nashode = fields.StringField(max_length=500, null=True)


class ISBNInfo(EmbeddedDocument):
    shabak = fields.StringField(max_length=500, null=True)
    vijegiha = fields.StringField(max_length=500, null=True)
    sharayet_tahie = fields.StringField(max_length=500, null=True)
    wrong_shabak = fields.StringField(max_length=500, null=True)


class DadeKoliPardazesh(EmbeddedDocument):
    dade_koli_pardazesh = fields.StringField(max_length=500, null=True)


class ZabanMadrak(EmbeddedDocument):
    zaban_matn = fields.StringField(max_length=500, null=True)
    zaban_asar_asli = fields.StringField(max_length=500, null=True)


class OnvanPadidavarande(EmbeddedDocument):
    onvan_asli = fields.StringField(max_length=500, null=True)
    nam_am_mavad = fields.StringField(max_length=500, null=True)
    onvan_movazi = fields.StringField(max_length=500, null=True)
    onvan_fari = fields.StringField(max_length=500, null=True)
    nam_nevisande = fields.StringField(max_length=500, null=True)
    virayeshgar = fields.StringField(max_length=500, null=True)


class VaziatVirast(EmbeddedDocument):
    vaziat_virast = fields.StringField(max_length=500, null=True)
    nam_avalin_virastar = fields.StringField(max_length=500, null=True)
    nam_sayer_virastar = fields.StringField(max_length=500, null=True)


class MoshakhasatZaher(EmbeddedDocument):
    shomare_safe = fields.StringField(max_length=500, null=True)
    sayer_joziat = fields.StringField(max_length=500, null=True)
    abad = fields.StringField(max_length=500, null=True)


class Froset(EmbeddedDocument):
    onvan_froset = fields.StringField(max_length=500, null=True)
    onvan_fari_froset = fields.StringField(max_length=500, null=True)
    shomare_froset = fields.StringField(max_length=500, null=True)


class YaddashtKoli(EmbeddedDocument):
    matn_yaddasht = fields.StringField(max_length=500, null=True)


class YaddashtOnvanPadidavarande(EmbeddedDocument):
    matn_yaddasht = fields.StringField(max_length=500, null=True)


class YaddashtVirastTarikhche(EmbeddedDocument):
    matn_yaddasht = fields.StringField(max_length=500, null=True)


class YaddashtNashrPakhsh(EmbeddedDocument):
    matn_yaddasht = fields.StringField(max_length=500, null=True)


class YaddashtMoshakhasatZaheri(EmbeddedDocument):
    matn_yaddasht = fields.StringField(max_length=500, null=True)


class YaddashtFroset(EmbeddedDocument):
    matn_yaddasht = fields.StringField(max_length=500, null=True)


class YaddashtOnvanhayeMortabet(EmbeddedDocument):
    matn_yaddasht = fields.StringField(max_length=500, null=True)


class KetabnameVajename(EmbeddedDocument):
    matn_yaddasht = fields.StringField(max_length=500, null=True)


class Mondarejat(EmbeddedDocument):
    matn_yaddasht = fields.StringField(max_length=500, null=True)


class YaddashtKholaseChekide(EmbeddedDocument):
    matn_yaddasht = fields.StringField(max_length=500, null=True)


class OnvanGharardadi(EmbeddedDocument):
    onvan_gharardadi = fields.StringField(max_length=500, null=True)
    onvan_gharardadi_taid_nashode = fields.StringField(max_length=500, null=True)


class OnvanAsliZabanDigar(EmbeddedDocument):
    onvan_zaban_digar = fields.StringField(max_length=500, null=True)
    onvan_fari = fields.StringField(max_length=500, null=True)


class OnvanRoJeld(EmbeddedDocument):
    onvan_ro_jeld = fields.StringField(max_length=500, null=True)
    onvan_fari = fields.StringField(max_length=500, null=True)


class OnvanAtf(EmbeddedDocument):
    onvan_atf = fields.StringField(max_length=500, null=True)
    onvan_fari = fields.StringField(max_length=500, null=True)


class OnvanDigar(EmbeddedDocument):
    onvan_gone_gon = fields.StringField(max_length=500, null=True)
    onvan_fari = fields.StringField(max_length=500, null=True)


class NamShakhsManzaleMozo(EmbeddedDocument):
    nam_khanevadegi = fields.StringField(max_length=500, null=True)
    nam = fields.StringField(max_length=500, null=True)
    tarikh_tavalod_vafat = fields.StringField(max_length=500, null=True)
    taghsim_fari_shekl = fields.StringField(max_length=500, null=True)
    taghsim_fari_mozoie = fields.StringField(max_length=500, null=True)
    taghsim_far_joghrafiaie = fields.StringField(max_length=500, null=True)
    taghsim_doreie = fields.StringField(max_length=500, null=True)
    nam_mostanad_nashode = fields.StringField(max_length=500, null=True)


class NamTanaleganManzaleMozo(EmbeddedDocument):
    nam_sazman = fields.StringField(max_length=500, null=True)
    shomare_hamayesh = fields.StringField(max_length=500, null=True)
    mahal_hamayesh = fields.StringField(max_length=500, null=True)
    tarikh_hamayesh = fields.StringField(max_length=500, null=True)
    taghsim_fari_shekli = fields.StringField(max_length=500, null=True)
    taghsim_fari_mozoie = fields.StringField(max_length=500, null=True)
    taghsim_far_joghrafiaie = fields.StringField(max_length=500, null=True)
    taghsim_doreie = fields.StringField(max_length=500, null=True)
    nam_mostanad_nashode = fields.StringField(max_length=500, null=True)


class OnvanManzaleMozo(EmbeddedDocument):
    onsor_shenaseie = fields.StringField(max_length=500, null=True)
    taghsim_fari_shekli = fields.StringField(max_length=500, null=True)


class NamJoghrafiaie(EmbeddedDocument):
    nam_joghrafiaie = fields.StringField(max_length=500, null=True)
    taghsim_fari_shekli = fields.StringField(max_length=500, null=True)
    taghsim_fari_mozoie = fields.StringField(max_length=500, null=True)
    taghsim_fari_joghrafiaie = fields.StringField(max_length=500, null=True)
    taghsim_fari_doreie = fields.StringField(max_length=500, null=True)
    mozo_mostanad_nashode = fields.StringField(max_length=500, null=True)


class Tosifgar(EmbeddedDocument):
    tosifgar = fields.StringField(max_length=500, null=True)


class DDCRating(EmbeddedDocument):
    rade_asli_fari = fields.StringField(max_length=500, null=True)
    neshane_asar = fields.StringField(max_length=500, null=True)
    marja = fields.StringField(max_length=500, null=True)


class KongereRating(EmbeddedDocument):
    rade_asli_fari = fields.StringField(max_length=500, null=True)
    shomare_kater = fields.StringField(max_length=500, null=True)
    marja = fields.StringField(max_length=500, null=True)


class NLMRating(EmbeddedDocument):
    rade_asli_fari = fields.StringField(max_length=500, null=True)
    shomare_kater = fields.StringField(max_length=500, null=True)
    marja = fields.StringField(max_length=500, null=True)


class Sarshenasname(EmbeddedDocument):
    nam_khanevadegi = fields.StringField(max_length=500, null=True)
    nam = fields.StringField(max_length=500, null=True)
    sal_tavalod_vafat = fields.StringField(max_length=500, null=True)
    naghsh = fields.StringField(max_length=500, null=True)
    nam_shakhs = fields.StringField(max_length=500, null=True)


class ShenaseAfzodeNevisandeHamkar(EmbeddedDocument):
    nam_khanevadegi = fields.StringField(max_length=500, null=True)
    nam = fields.StringField(max_length=500, null=True)
    sal_tavalod_vafat = fields.StringField(max_length=500, null=True)
    naghsh = fields.StringField(max_length=500, null=True)
    nam_shakhs = fields.StringField(max_length=500, null=True)


class ShenaseAfzodeSayerPadidavaran(EmbeddedDocument):
    nam_khanevadegi = fields.StringField(max_length=500, null=True)
    nam = fields.StringField(max_length=500, null=True)
    sal_tavalod_vafat = fields.StringField(max_length=500, null=True)
    naghsh = fields.StringField(max_length=500, null=True)
    nam_shakhs = fields.StringField(max_length=500, null=True)


class SarshenaseNamTanalegan(EmbeddedDocument):
    nam_confrans = fields.StringField(max_length=500, null=True)
    nam_fari_sazman = fields.StringField(max_length=500, null=True)
    shomare_hamayesh = fields.StringField(max_length=500, null=True)
    mahal_hamayesh = fields.StringField(max_length=500, null=True)
    tarikh_hamayesh = fields.StringField(max_length=500, null=True)
    naghsh = fields.StringField(max_length=500, null=True)
    nam_sazman = fields.StringField(max_length=500, null=True)


class ShenaseAfzodeNevisandeHamkarNamTanalegan(EmbeddedDocument):
    nam_sazman = fields.StringField(max_length=500, null=True)
    nam_fari_sazman = fields.StringField(max_length=500, null=True)
    naghsh = fields.StringField(max_length=500, null=True)
    nam_sazman_mostanad_nashode = fields.StringField(max_length=500, null=True)


class ShenaseAfzodeNamTanalegan(EmbeddedDocument):
    nam_sazman = fields.StringField(max_length=500, null=True)
    nam_fari_sazman = fields.StringField(max_length=500, null=True)
    naghsh = fields.StringField(max_length=500, null=True)
    nam_sazman_mostanad_nashode = fields.StringField(max_length=500, null=True)


class MabdaAsli(EmbeddedDocument):
    keshvar = fields.StringField(max_length=500, null=True)
    sazman = fields.StringField(max_length=500, null=True)


class DastresiMahalElectronic(EmbeddedDocument):
    nam_file_enteghal_dade_shode = fields.StringField(max_length=500, null=True)
    nam_electronic = fields.StringField(max_length=500, null=True)


class EtelaatFehrestNevisOperator(EmbeddedDocument):
    nam_fehrest_nevis = fields.StringField(max_length=500, null=True)
    chekide_nevis = fields.StringField(max_length=500, null=True)
    nam_panchist = fields.StringField(max_length=500, null=True)
    tarikh_fehrest_nevisi = fields.StringField(max_length=500, null=True)
    nam_khanevadegi_operator = fields.StringField(max_length=500, null=True)
    nam_operator = fields.StringField(max_length=500, null=True)
    tarikh_vorod_etelaat = fields.StringField(max_length=500, null=True)
    nam_bazbin_nahaie = fields.StringField(max_length=500, null=True)
    sarparast_fehrest_nevisi = fields.StringField(max_length=500, null=True)
    tarikh_control = fields.StringField(max_length=500, null=True)
    tarikh_vorod = fields.StringField(max_length=500, null=True)


class VaziatEnteshar(EmbeddedDocument):
    format_enteshar = fields.StringField(max_length=500, null=True)


class EtelaatRecordKetabshenasi(EmbeddedDocument):
    noe_made = fields.StringField(max_length=500, null=True)
    code_karbarge = fields.StringField(max_length=500, null=True)


class EtelaatDastresiRecord(EmbeddedDocument):
    takmil_shode = fields.StringField(max_length=500, null=True)
    sath_dastresi = fields.StringField(max_length=500, null=True)


class FehrestMondarejat(EmbeddedDocument):
    mondarejat = fields.StringField(max_length=50, null=True)


class BookInventory(EmbeddedDocument):
    total = fields.IntField(min_value=0, null=True)
    available = fields.IntField(min_value=0, null=True)

    def save(self, *args, **kwargs):
        if self.total < self.available:
            raise ValidationError('مقدار کتاب‌های موجود نمیتواند از کل موجودی این کتاب بیشتر باشد.')
        return super(BookInventory, self).save(*args, **kwargs)


class Title(EmbeddedDocument):
    fa = fields.StringField(max_length=500, null=True)
    en = fields.StringField(max_length=500, null=True)


class ShomareMadrak(EmbeddedDocument):
    shomare_madrak = fields.StringField(max_length=500, null=True)


class Book(Document):
    shomare_madrak = fields.EmbeddedDocumentField(ShomareMadrak)
    nasher_specs = fields.EmbeddedDocumentField(Publisher)
    title = fields.EmbeddedDocumentField(Title)
    record_id = fields.StringField(max_length=20)
    isbn = fields.StringField(max_length=13, required=False, null=True)
    shabak = fields.EmbeddedDocumentField(ISBNInfo)
    dade_koli_pardazesh = fields.EmbeddedDocumentField(DadeKoliPardazesh)
    onvan_padidavarande = fields.EmbeddedDocumentField(OnvanPadidavarande)
    zaban_madrak = fields.EmbeddedDocumentField(ZabanMadrak, null=True, required=False)
    vaziat_virast = fields.EmbeddedDocumentField(VaziatVirast)
    moshakhasat_zaheri = fields.EmbeddedDocumentField(MoshakhasatZaher)
    froset = fields.EmbeddedDocumentField(Froset)
    yaddasht_koli = fields.EmbeddedDocumentField(YaddashtKoli)
    yaddasht_onvan_padidavarande = fields.EmbeddedDocumentField(YaddashtOnvanPadidavarande)
    yaddasht_virast_tarikhche = fields.EmbeddedDocumentField(YaddashtVirastTarikhche)
    yaddasht_nashr_pakhsh = fields.EmbeddedDocumentField(YaddashtNashrPakhsh)
    yaddasht_moshakhasat_zaheri = fields.EmbeddedDocumentField(YaddashtMoshakhasatZaheri)
    yaddasht_froset = fields.EmbeddedDocumentField(YaddashtFroset)
    yaddasht_onvanhaye_mortabet = fields.EmbeddedDocumentField(YaddashtOnvanhayeMortabet)
    ketabname_vajename = fields.EmbeddedDocumentField(KetabnameVajename)
    mondarejat = fields.EmbeddedDocumentField(Mondarejat)
    yaddasht_kholase_chekide = fields.EmbeddedDocumentField(YaddashtKholaseChekide)
    onvan_gharardadi = fields.EmbeddedDocumentField(OnvanGharardadi)
    onvan_asli_zaban_digar = fields.EmbeddedDocumentField(OnvanAsliZabanDigar)
    onvan_ro_jeld = fields.EmbeddedDocumentField(OnvanRoJeld)
    onvan_atf = fields.EmbeddedDocumentField(OnvanAtf)
    onvan_digar = fields.EmbeddedDocumentField(OnvanDigar)
    nam_shakhs_manzale_mozo = fields.EmbeddedDocumentField(NamShakhsManzaleMozo)
    nam_tanalegan_manzale_mozo = fields.EmbeddedDocumentField(NamTanaleganManzaleMozo)
    onvan_manzale_mozo = fields.EmbeddedDocumentField(OnvanManzaleMozo)
    mozo = fields.EmbeddedDocumentField(Mozo)
    nam_joghrafiaie = fields.EmbeddedDocumentField(NamJoghrafiaie)
    tosifgar = fields.EmbeddedDocumentField(Tosifgar)
    radebandi_ddc = fields.EmbeddedDocumentField(DDCRating)
    radebandi_kongere = fields.EmbeddedDocumentField(KongereRating)
    radebandi_nlm = fields.EmbeddedDocumentField(NLMRating)
    sarshenasname = fields.EmbeddedDocumentField(Sarshenasname)
    shenase_afzode_nevisande_hamkar = fields.EmbeddedDocumentField(ShenaseAfzodeNevisandeHamkar)
    shenase_afzode_sayer_padidavaran = fields.EmbeddedDocumentField(ShenaseAfzodeSayerPadidavaran)
    sarshenase_nam_tanalegan = fields.EmbeddedDocumentField(SarshenaseNamTanalegan)
    shenase_afzode_nevisande_hamkar_nam_tanalegan = fields.EmbeddedDocumentField(
        ShenaseAfzodeNevisandeHamkarNamTanalegan)
    shenase_afzode_nam_tanalegan = fields.EmbeddedDocumentField(ShenaseAfzodeNamTanalegan)
    mabda_asli = fields.EmbeddedDocumentField(MabdaAsli)
    dastresi_mahal_electronic = fields.EmbeddedDocumentField(DastresiMahalElectronic)
    etelaat_fehrest_nevis_operator = fields.EmbeddedDocumentField(EtelaatFehrestNevisOperator)
    vaziat_enteshar = fields.EmbeddedDocumentField(VaziatEnteshar)
    etelaat_record_ketabshenasi = fields.EmbeddedDocumentField(EtelaatRecordKetabshenasi)
    etelaat_dastresi_record = fields.EmbeddedDocumentField(EtelaatDastresiRecord)
    fehrest_mondarejat = fields.EmbeddedDocumentField(FehrestMondarejat)
    slug = fields.StringField(max_length=50, unique=True)
    book_inventory = fields.EmbeddedDocumentField(BookInventory)
    reservable = fields.BooleanField(required=True, default=True)
    created = fields.DateTimeField()

    def save(self, force_insert=False, validate=True, clean=True,
             write_concern=None, cascade=None, cascade_kwargs=None,
             _refs=None, save_condition=None, signal_kwargs=None, **kwargs):
        tarikh = getattr(self.nasher_specs, 'tarikh', None)
        if tarikh:
            self.nasher_specs.tarikh = self.parse_tarikh(tarikh)
        if not self.created:
            self.created = datetime.now()
        if not self.slug:
            self.slug = generate_slug(self)
        # Todo : CHECK THIS IT should be removed
        if not self.id:
            if self.book_inventory:
                if not self.book_inventory.total:
                    self.book_inventory.total = 1
                if not self.book_inventory.available:
                    self.book_inventory.available = 1
        if not self.book_inventory:
            self.book_inventory = BookInventory(total=1, available=1)
        return super(Book, self).save(force_insert, validate, clean, write_concern, cascade, cascade_kwargs, _refs,
                                      save_condition, signal_kwargs, **kwargs)

    def lend_book(self, val):
        """
        function to lend multiple books at once if the there is enough books.
        :raise validationError if there is not enough books
        :param val:
        :return:
        """
        if not self.book_inventory:
            raise ValidationError('موجودی برای این کتاب تعریف نشده است.')
        if self.book_inventory.available >= val:
            self.book_inventory.available -= val
            self.save()
        else:
            raise ValidationError("کتابی برای امانت در حال حاضر موجود نیست")
        return True

    def return_book(self, val):
        """
        function to return a book to inventory
        :raise Validation Error when all the books are in inventory and u try to return more!
        :return:
        """
        if not self.book_inventory:
            raise ValidationError('موجودی برای این کتاب تعریف نشده است.')
        if self.book_inventory.available == self.book_inventory.total:
            raise ValidationError("تمام کتاب ها در کتاب‌خانه‌اند ،نمیتوان کتابی برگرداند.")
        self.book_inventory.available += val
        self.save()
        return True

    def check_book_availability(self):
        if self.reservable:
            if self.book_inventory.available > 0:
                return 'o'
            return 'q'
        return False

    def parse_tarikh(self, text):
        if type(text) == int:
            return text
        year = ''
        for ch in text:
            if ch.isdigit():
                year += ch
        if year:
            year = int(year)
        return year


class UserBook(models.Model):
    """
    model to lend and return books, when a user borrows a book a record of this table
    is created to track the book status. Automatically calculates the penalty if the user
    returns the books after the specified date.
    """
    book = models.CharField(max_length=100)
    user = models.ForeignKey(User, related_name='books')
    borrow_start_date = jmodels.jDateField(null=True, blank=True)
    borrow_return_date = jmodels.jDateField(null=True, blank=True)
    penalty = models.FloatField(default=0)
    renewed = models.BooleanField(default=False)
    created = jmodels.jDateTimeField(auto_now_add=True)
    tracking_code = models.CharField(max_length=15, null=True, blank=True)
    active = models.BooleanField(default=True)
    REQUEST_STATUS = (
        ('canceled', 'لغو شده'),
        ('approved', 'تایید شده'),
        ('pending', 'در انتظار تایید'),
        ('queue', 'در صف انتظار'),
        ('returned', 'تحویل داده شده'),
    )
    status = models.CharField(max_length=15, choices=REQUEST_STATUS, default='pending')

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.tracking_code:
            self.tracking_code = generate_tracking_code()

        return super(UserBook, self).save(force_insert, force_update, using, update_fields)

    def __unicode__(self):
        return self.user.username

    def return_book(self):
        book = Book.objects.filter(slug=self.book).first()
        book.return_book(1)
        if not self.status == 'approved':
            raise ValidationError('درخواست برای این کتاب تایید نشده است.')
        self.borrow_return_date = jdatetime.datetime.now().date()
        self.active = False
        self.status = 'returned'
        self.save()
        penalty = self.calculate_penalty()
        print penalty
        if penalty:
            self.penalty = penalty
        self.save()

    def approve_request(self):
        """
        Approve the user request for the book if its pending if its not in pending state it will raise an exception
        """
        if self.status == 'pending' or self.status == 'queue':
            book = Book.objects.filter(slug=self.book).first()
            if not book:
                raise ValidationError('کتاب وارد شده وحود ندارد.')
            if self.status == 'queue':
                book.lend_book(1)
            self.status = 'approved'
            self.borrow_start_date = datetime.now().date()
            self.save()
            return True
        else:
            raise ValidationError('درخواست از قبل تایید یا رد شده بوده است.')

    def cancel_request(self):
        if self.status == 'pending' or self.status == 'queue':
            if self.status == 'pending':
                book = Book.objects.filter(slug=self.book).first()
                book.return_book(1)
            self.status = 'canceled'
            self.active = False
            self.save()
            return True
        else:
            raise ValidationError("درخواست قابل لغو شدن نیست.")

    def calculate_penalty(self):
        """
        calculates the penalty for a book based on the user type
        """
        delta = self.borrow_return_date - self.borrow_start_date
        user_days = self.user.user_type.days
        if self.renewed:
            user_days *= 2
        if delta.days > user_days:
            return delta.days - user_days
        return False


class Maghale(models.Model):
    onvan = models.CharField(max_length=500)
    nevisande = models.CharField(max_length=200)
    nevisande_hamkar = models.CharField(max_length=200, null=True, blank=True)
    majale = models.CharField(max_length=200, null=True, blank=True)
    tarikh = jmodels.jDateField()
    mozo = models.CharField(max_length=500)
    file = models.FileField(upload_to=maghale_images_upload_address, blank=True, null=True)
    created = jmodels.jDateTimeField(auto_now_add=True, null=True)

    def __unicode__(self):
        return self.onvan


class Tarh(models.Model):
    onvan = models.CharField(max_length=500)
    nevisande = models.CharField(max_length=200)
    nevisande_hamkar = models.CharField(max_length=200, null=True, blank=True)
    tarikh = jmodels.jDateField()
    mozo = models.CharField(max_length=500)
    created = jmodels.jDateTimeField(auto_now_add=True, null=True)

    def __unicode__(self):
        return self.onvan


class UserFavourite(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='favourite')
    slug = models.CharField(max_length=100)
    type_choise = (('book', 'کتاب'),
                   ('maghale', 'مقاله'),
                   ('tarh', 'طرح'))
    type = models.CharField(choices=type_choise, default='book', max_length=10)

    def __unicode__(self):
        return '{} : {}: {}'.format(self.user.username, self.slug, self.type)

    class Meta:
        unique_together = ('user', 'slug', 'type')
