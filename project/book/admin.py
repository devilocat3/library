# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import UserBook, Maghale, Tarh, UserFavourite
# Register your models here.
admin.site.register(UserBook)
admin.site.register(Maghale)
admin.site.register(Tarh)
admin.site.register(UserFavourite)