# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.core.validators import RegexValidator
from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.utils import timezone
from PIL import Image
from fcm_django.models import FCMDevice
from django_jalali.db import models as jmodels
from fcm_django.utils import send_notification_to_user

VIEW_BOOKS_PERMISSION = 'view_books'
MANAGE_BOOKS_PERMISSION = 'manage_books'
VIEW_USER_PERMISSION = 'view_user'
MANAGE_USERS_PERMISSION = 'manage_users'
VIEW_USER_BOOKS_PERMISSION = 'view_user_books'
MANAGE_USER_BOOKS_PERMISSION = 'manage_user_books'
VIEW_LOGS_PERMISSION = 'view_logs'
MANAGE_LOGS_PERMISSION = 'manage_logs'
MANAGE_MAGHALES_PERMISSION = 'manage_maghale'
MANAGE_TARH_PERMISSION = 'manage_tarh'

PERMISSIONS = (
    (VIEW_BOOKS_PERMISSION, 'مشاهده لیست کتاب های موجود در کتابخانه'),
    (MANAGE_BOOKS_PERMISSION, 'مدیریت کتاب‌های موجود در کتاب‌خانهَ، اضافه کردن، حذف کردن و تغییر کتاب'),
    (VIEW_USER_PERMISSION, 'مشاهده اطلاعات کاربرها مثل نام و نام خانوادگی، گروه، وضعیت لاگین و غیره'),
    (MANAGE_USERS_PERMISSION,
     'مدیریت کاربران شامل ایجاد کاربر جدید، ویرایش یا حذف کاربران موجود، فعال/غیرفعال کاربر و غیره'),
    (VIEW_USER_BOOKS_PERMISSION, 'مشاهده رزوهای کاربران'),
    (MANAGE_USER_BOOKS_PERMISSION, 'مدیریت رزروهای کاربران مثل تایید یا لغو انها'),
    (VIEW_LOGS_PERMISSION, 'مشاهده لاگ‌های ثبت شده توسط کاربران'),
    (MANAGE_LOGS_PERMISSION, 'مدیریت لاگ‌های ثبت شده توسط کاربران'),
    (MANAGE_MAGHALES_PERMISSION, 'مدیریت مقالات کتاب خانه'),
    (MANAGE_TARH_PERMISSION, 'مدیریت طرح‌های کتاب‌خانه')
)


def avatar_images_upload_address(instance, filename):
    upload_dir = "img/avatar/%s" % filename
    return upload_dir


class UserType(models.Model):
    days = models.IntegerField()
    number_of_books = models.IntegerField()
    can_renew_book = models.BooleanField()
    penalty = models.IntegerField(default=100)
    TYPES = (
        ('employee', 'کارمند'),
        ('faculty_member', 'هیئت علمی'),
        ('masters', 'ارشد'),
        ('phd', 'دکترا'),
        ('bachelor', 'دانشجو کارشناسی')
    )
    type_of = models.CharField(choices=TYPES, default='bachelor', max_length=20, unique=True)

    def __unicode__(self):
        return self.type_of


class UserManager(BaseUserManager):
    def _create_user(self, username, password, is_staff, is_superuser, **extra_fields):
        now = timezone.now()
        if not username:
            raise ValueError('نام کاربری وارد نشده است')
        user = self.model(username=username,
                          is_staff=is_staff, is_active=False,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create(self, username, password=None, **extra_fields):
        user = self._create_user(username, password, False, False, **extra_fields)
        user.is_active = False
        # print(user.identifier_number)

        user.save()
        return user

    def create_staff(self, username, password, **extra_fields):
        user_type = UserType.objects.get(type_of='employee')
        user = self._create_user(username, password, True, False, user_type=user_type, **extra_fields)
        user.is_active = True
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password, **extra_fields):
        user = self._create_user(username, password, True, True, **extra_fields)
        user.is_active = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=30, unique=True, validators=[RegexValidator(regex='^[\w.@+-]+$')])
    first_name = models.CharField(max_length=30, blank=True, null=True)
    last_name = models.CharField(max_length=30, blank=True, null=True)
    email = models.EmailField(max_length=255, null=True, unique=True)
    user_type = models.ForeignKey(UserType, related_name='user', null=True)
    # identifier_number = models.CharField(max_length=15, validators=[RegexValidator(regex='^[1-9][0-9]{4,14}$')],
    #                                      unique=True)
    national_id = models.CharField(max_length=10, default='0000000000')
    university = models.CharField(max_length=255, null=True, blank=True)
    GENDER = (
        ('male', 'مرد'),
        ('female', 'زن')
    )
    gender = models.CharField(choices=GENDER, default='male', null=True, blank=True, max_length=10)
    address = models.TextField(null=True, blank=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    date_joined = jmodels.jDateTimeField(auto_now_add=True)
    birth_date = jmodels.jDateField(null=True, blank=True)
    USERNAME_FIELD = 'username'
    # REQUIRED_FIELDS = ['identifier_number', ]

    objects = UserManager()

    mobile = models.CharField(max_length=15, unique=True, null=True, blank=True, validators=[
        RegexValidator(regex='^\+?1?\d{9,15}$', message='شماره موبایل معتبر نمی‌باشد', )
    ])

    avatar = models.ImageField(upload_to=avatar_images_upload_address, blank=True,
                               null=True, max_length=1500, verbose_name='عکس پروفایل')

    lookup_id = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'
        unique_together = ('username', 'mobile')
        db_table = 'user'

    def get_full_name(self):
        if not self.first_name or not self.last_name:
            full_name = self.username
        else:
            full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def activate(self):
        self.is_active = True
        self.save()
        devices = FCMDevice.objects.filter(user=self)
        msg = u'کاربر {} حساب شما توسط مدیریت فعال شد.'.format(self.username)
        send_notification_to_user(self, title=u'تایید عضویت', msg=msg, type="user")

    def deactivate(self):
        self.is_active = False
        self.save()
        msg = u'کاربر {} حساب شما توسط مدیریت غیر فعال شد.'.format(self.username)
        send_notification_to_user(self, title=u'تایید عضویت', msg=msg, type="user")

    def get_short_name(self):
        return self.first_name

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)

        if self.id is not None:
            if self.avatar:
                avatar = Image.open(self.avatar.path)
                avatar = avatar.resize((200, 200), Image.ANTIALIAS)
                try:
                    avatar.save(self.avatar.path)
                except:
                    avatar.save(self.avatar.path + ".png")
                    self.avatar = avatar

    def __unicode__(self):
        return self.username


class RolePermission(models.Model):
    name = models.CharField(choices=PERMISSIONS, max_length=50, unique=True)

    def __unicode__(self):
        return unicode(self.name)


class Role(models.Model):
    name = models.CharField(max_length=50, unique=True)
    permissions = models.ManyToManyField('RolePermission')
    updated_at = jmodels.jDateTimeField(auto_now=True)

    def __unicode__(self):
        return unicode(self.name)

    def assign_perms(self, perms_list):
        for p in perms_list:
            permission = RolePermission.objects.get_or_create(name=p)[0]
            self.permissions.add(permission)
        self.save()


class UserRole(models.Model):
    user = models.OneToOneField(User, related_name='user_role')
    role = models.ForeignKey(Role)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.user.username == 'AnonymousUser':
            raise ValueError("can't save anything for this user")
        elif not self.user.is_staff:
            raise ValueError("can't give role to normal users")

        return super(UserRole, self).save(force_insert, force_update, using, update_fields)

    def __unicode__(self):
        return '{}, {}'.format(self.user.username, self.role.name)

    def get_perms(self):
        perms = self.role.permissions.all()
        return perms

    def has_perm(self, perm):
        perm = RolePermission.objects.filter(name=perm).first()
        if perm in self.role.permissions.all():
            return True
        return False
