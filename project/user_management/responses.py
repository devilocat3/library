from rest_framework.response import Response


class CustomResponse(Response):
    def __init__(self, data=None, message=None, status=None,
                 template_name=None, headers=None,
                 exception=False, content_type=None):
        data = {
            "message": message,
            "status": 0,
            "data": data
        }
        super(CustomResponse, self).__init__(data=data, status=status, template_name=template_name, headers=headers, exception=exception, content_type=content_type )
