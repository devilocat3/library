# -*- coding: utf-8 -*-
from django_filters import rest_framework as filters
from .models import User, UserRole
from dashboard.jalali import convert_string_to_jdate
GENDER = (
    ('male', 'male'),
    ('female', 'female')
)

TYPES = (
    ('employee', 'کارمند'),
    ('faculty_member', 'هیئت علمی'),
    ('masters', 'ارشد'),
    ('phd', 'دکترا'),
    ('bachelor', 'دانشجو کارشناسی')
)


class UserFilters(filters.FilterSet):
    username = filters.CharFilter(name="username", lookup_expr='icontains')
    fname = filters.CharFilter(name="first_name", lookup_expr='icontains')
    lname = filters.CharFilter(name="last_name", lookup_expr='icontains')
    gender = filters.ChoiceFilter(name="gender", choices=GENDER)
    type = filters.ChoiceFilter(name="user_type__type_of", choices=TYPES)
    date_from = filters.CharFilter(method='custom_date_filter', name='date_joined__gte')
    date_to = filters.CharFilter(method='custom_date_filter', name="date_joined__lte")

    class Meta:
        model = User
        fields = ['username', 'fname', 'lname', 'gender', 'date_to', 'date_from']

    def custom_date_filter(self, qs, name, value):
        jtime = convert_string_to_jdate(value)
        qs = qs.filter(**{name: jtime})
        return qs


class StaffFilters(filters.FilterSet):
    username = filters.CharFilter(name="username", lookup_expr='icontains')
    fname = filters.CharFilter(name="first_name", lookup_expr='icontains')
    lname = filters.CharFilter(name="last_name", lookup_expr='icontains')
    gender = filters.ChoiceFilter(name="gender", choices=GENDER)
    type = filters.ChoiceFilter(name="user_type__type_of", choices=TYPES)
    date_joined = filters.DateTimeFilter(name='date_joined', lookup_expr='gte')
    role = filters.CharFilter(name='user_role__role__name', lookup_expr='icontains')

    class Meta:
        model = User
        fields = ['username', 'fname', 'lname', 'gender']


class UserRoleFilters(filters.FilterSet):
    username = filters.CharFilter(name='user__username', lookup_expr='exact')
    role = filters.CharFilter(name='role__name', lookup_expr='exact')

    class Meta:
        model = UserRole
        fields = ['username', 'role']
