# -*- coding: utf-8 -*-
import jwt
import uuid
from rest_framework.exceptions import ValidationError

from rest_framework_jwt.compat import get_username
from rest_framework_jwt.compat import get_username_field
from rest_framework_jwt.settings import api_settings
from datetime import datetime
from calendar import timegm
from rest_framework_jwt.utils import jwt_get_secret_key

from .models import User


def jwt_payload_handler(user):
    username_field = get_username_field()
    username = get_username(user)
    if user.is_staff:
        user_type = 'staff'
    else:
        user_type = 'normal'
    payload = {
        'user_id': user.pk,
        'username': username,
        'exp': datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA,
        'type': user_type,
        'lookup_id': user.lookup_id
    }
    if hasattr(user, 'email'):
        payload['email'] = user.email
    if isinstance(user.pk, uuid.UUID):
        payload['user_id'] = str(user.pk)

    payload[username_field] = username

    # Include original issued at time for a brand new token,
    # to allow token refresh
    if api_settings.JWT_ALLOW_REFRESH:
        payload['orig_iat'] = timegm(
            datetime.utcnow().utctimetuple()
        )

    if api_settings.JWT_AUDIENCE is not None:
        payload['aud'] = api_settings.JWT_AUDIENCE

    if api_settings.JWT_ISSUER is not None:
        payload['iss'] = api_settings.JWT_ISSUER

    return payload


def jwt_decode_handler(token):
    options = {
        'verify_exp': api_settings.JWT_VERIFY_EXPIRATION,
    }
    # get user from token, BEFORE verification, to get user secret key
    unverified_payload = jwt.decode(token, None, False)
    secret_key = jwt_get_secret_key(unverified_payload)
    data = jwt.decode(
        token,
        api_settings.JWT_PUBLIC_KEY or secret_key,
        api_settings.JWT_VERIFY,
        options=options,
        leeway=api_settings.JWT_LEEWAY,
        audience=api_settings.JWT_AUDIENCE,
        issuer=api_settings.JWT_ISSUER,
        algorithms=[api_settings.JWT_ALGORITHM]
    )
    user = User.objects.filter(id=data.get('user_id')).first()
    if not user:
        raise ValidationError(u'کاربر یافت نشد.')
    if not user.lookup_id == data.get('lookup_id'):
        raise ValidationError(u'توکن معتبر نمی‌باشد.')
    return data

def convert_persian_digit(text):
    if not type(text) == unicode:
        text = text.decode('utf-8')
    for ch in text:
        try:
            ch1 = str(int(ch))
            text = text.replace(ch, ch1)
        except:
            pass
    return text