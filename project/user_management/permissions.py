from rest_framework.permissions import BasePermission
from .models import *


class IsAdminUser(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_superuser


class IsAdminOrHasPermission(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        role = getattr(request.user, 'user_role', None)
        if role:
            perms = role.get_perms()
            needed_permissions = view.needed_permissions
            has_perm = perms.filter(name__in=needed_permissions)
            if has_perm:
                return True
        return False


class IsOwner(BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser:
            return True
        role = getattr(request.user, 'user_role', None)
        if role:
            perms = role.get_perms()
            needed_permissions = view.needed_permissions
            has_perm = perms.filter(name__in=needed_permissions)
            if has_perm:
                return True
        if isinstance(obj, User):
            return obj == request.user
        return obj.user == request.user
