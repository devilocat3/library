# -*- coding: utf-8 -*-
from django.core.management import BaseCommand

from user_management.models import PERMISSIONS, RolePermission, Role

ROLES = ['فهرست نویس', 'خزانه']


class Command(BaseCommand):
    def handle(self, *args, **options):
        for permission in PERMISSIONS:
            perm, created = RolePermission.objects.get_or_create(name=permission[0])
            if created:
                print '[+] Permission created: {}'.format(perm.name)
            else:
                print '[-] Permission existed: {}'.format(perm.name)
        print '=========================================================='
        perms = RolePermission.objects.all()
        role, created = Role.objects.get_or_create(name='همه‌کاره')
        if created:
            role.permissions = perms
            role.save()
            print '[+] Role Hamekare created'
        else:
            print '[-] Role Hamekare existed'
