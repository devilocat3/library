# -*- coding: utf-8 -*-
from django.contrib.auth.models import User, Group
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.core.management import BaseCommand

from user_management.models import *

TYPES = (
    ('employee', 'کارمند'),
    ('heiatelmi', 'هیئت علمی'),
    ('arshad', 'ارشد'),
    ('doctor', 'دکترا'),
    ('karshenasi', 'دانشجو کارشناسی')
)


class Command(BaseCommand):

    def handle(self, *args, **options):
        heiat = UserType.objects.create(type_of='faculty_member', number_of_books=8, days=130, can_renew_book=True)
        arshad = UserType.objects.create(type_of='masters', number_of_books=3, days=30, can_renew_book=True)
        doctor = UserType.objects.create(type_of='phd', number_of_books=4, days=21, can_renew_book=True)
        karshenasi = UserType.objects.create(type_of='bachelor', number_of_books=2, days=14, can_renew_book=True)
        employee = UserType.objects.create(type_of='employee', number_of_books=2, days=14, can_renew_book=False)
        print("created user_types")
        a = User.objects.create('test', 'test1234', user_type=karshenasi)
        b = User.objects.create('test1', 'test1234', user_type=arshad)
        c = User.objects.create('test2', 'test1234', user_type=employee)
        d = User.objects.create('test3', 'test1234', user_type=doctor)
        e = User.objects.create('test4', 'test1234', user_type=heiat)
        a.is_active = True
        b.is_active = True
        c.is_active = True
        d.is_active = True
        e.is_active = True
        a.save()
        b.save()
        c.save()
        d.save()
        e.save()
        print("created test users")
        staff_test1 = User.objects.create_staff(username='staff', password='test1234')
        staff_test2 = User.objects.create_staff(username='staff2', password='test1234')
        admin = User.objects.create_superuser('admin', 'test1234')
        print("admin created")
