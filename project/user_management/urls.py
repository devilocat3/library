from rest_framework.routers import DefaultRouter
from django.conf.urls import url, include
from .views import UserViewSet, CustomObtainJSONWebToken, RoleManagementView, UserRoleView, NormalObtainJSONWebToken
from .views import UserTypeViewSet
router = DefaultRouter()

router.register(r'users', UserViewSet)
router.register(r'role', RoleManagementView)
router.register(r'user-role', UserRoleView)
router.register(r'user-type', UserTypeViewSet)

urlpatterns = [
    url(r'', include(router.urls)),
    url(r'api-token-get$', CustomObtainJSONWebToken.as_view()),
    url(r'api-token-get/normal', NormalObtainJSONWebToken.as_view())
]