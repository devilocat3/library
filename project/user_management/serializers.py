# -*- coding: utf-8 -*-
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from book.models import UserBook
import jdatetime
from .models import User, UserRole, RolePermission, Role, UserType

USER_TYPES = ['employee', 'faculty_member', 'masters', 'phd', 'bachelor']


class UserSerializer(serializers.ModelSerializer):
    username = serializers.RegexField(required=True, regex=r'^[1-9][0-9]{4,14}$')
    user_type = serializers.ChoiceField(choices=USER_TYPES, required=True)
    last_login = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'is_active', 'user_type', 'id', 'address', 'gender', 'email',
                  'password', 'mobile', 'avatar', 'date_joined', 'national_id', 'is_superuser',
                  'last_login', 'birth_date', 'university']
        read_only_fields = ['date_joined', 'is_active', 'id', 'is_superuser', 'last_login']
        extra_kwargs = {
            'password': {'write_only': True}, 'user_type': {'required': True}
        }

    def get_last_login(self, obj):
        if not obj.last_login:
            return None
        jtime = jdatetime.datetime.fromgregorian(datetime=obj.last_login)
        return jtime.strftime("%Y-%m-%d %H:%M:%S")

    def validate(self, attrs):
        user_type = attrs.get('user_type')
        u_type = UserType.objects.filter(type_of=user_type).first()
        attrs['user_type'] = u_type
        return attrs

    def to_representation(self, instance):
        data = super(UserSerializer, self).to_representation(instance)
        if instance.avatar:
            data.update({'avatar': self.context['request'].build_absolute_uri(instance.avatar.url)})
        user_type = getattr(instance, 'user_type', None)
        if user_type:
            reserves = UserBook.objects.filter(status='returned', user=instance)
            penalty_days = 0
            for reserve in reserves:
                penalty_days += reserve.penalty
            penalty = penalty_days * user_type.penalty
            data.update({
                "user_type": {
                    "number_of_books": user_type.number_of_books,
                    "can_renew_book": user_type.can_renew_book,
                    "days": user_type.days,
                    "penalty": penalty,
                    "type_of": user_type.type_of
                }
            })
        user_role = getattr(instance, 'user_role', None)
        if instance.is_superuser:
            data.update({'user_role': {
                'name': u'مدیر سیستم'
            }})
        if user_role:
            data.update({'user_role': {
                'id': user_role.id,
                'name': user_role.role.name
            }})
        return data

    def create(self, validated_data):
        username = validated_data['username']
        if User.objects.filter(username=username).first():
            raise ValidationError(u'نام کاربری نمی‌تواند تکراری باشد.')
        return super(UserSerializer, self).create(validated_data)

class StaffCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'first_name', 'last_name', 'email', 'mobile', 'last_login',]
        read_only_fields = ['id', 'last_login']
    # def validate(self, attrs):
    #     username = attrs['username']
    #     if User.objects.filter(identifier_number=username).first():
    #         raise ValidationError("نام کاربری معتبر نمی‌باشد.")

class ApproveSerializer(serializers.Serializer):
    username = serializers.CharField()
    activate = serializers.BooleanField(default=True)


class RolePermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = RolePermission
        fields = ['name']


class RoleSerializer(serializers.ModelSerializer):
    permissions_list = serializers.ListField(required=False, write_only=True)

    class Meta:
        model = Role
        fields = '__all__'
        read_only_fields = ['updated', 'id', 'permissions']

    def create(self, validated_data):
        perms_list = validated_data.get('permissions_list', None)
        if perms_list:
            perms = RolePermission.objects.filter(name__in=perms_list)
            if len(perms) != len(perms_list):
                raise ValidationError('شناسه وارده معتبر نمی‌باشد.')
        else:
            perms = None
        try:
            role = Role.objects.create(name=validated_data.get('name'))
        except:
            raise ValidationError('نقشی با این نام ساخته شده است.')
        if perms:
            role.permissions = perms
            role.save()

        return role

    def update(self, instance, validated_data):
        perms_list = validated_data.get('permissions_list', None)
        name = validated_data.get('name', None)
        if perms_list:
            perms = RolePermission.objects.filter(name__in=perms_list)
            if len(perms) != len(perms_list):
                raise ValidationError('شناسه وارده معتبر نمی‌باشد.')
            instance.permissions = perms
        if name:
            instance.name = name
        instance.save()
        return instance

    def to_representation(self, instance):
        context = super(RoleSerializer, self).to_representation(instance)
        perms = instance.permissions.all()
        perm_list = []
        for perm in perms:
            perm_list.append(perm.name)
        context['permissions'] = perm_list
        return context


class UserRoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserRole
        fields = '__all__'
        read_only_fields = ['id']

    def to_representation(self, instance):
        context = {
            "id": instance.id,
            "user": {
                "username": instance.user.username,
                "id": instance.user.id,
            },
            "role": {
                "id": instance.role.id,
                "name": instance.role.name,
            }
        }
        return context


class UserRoleUpsertSerializer(serializers.Serializer):
    user = serializers.IntegerField(required=True)
    role = serializers.IntegerField(required=True)

    def validate(self, attrs):
        user = User.objects.filter(id=attrs.get('user')).first()
        if not user:
            raise ValidationError(u'کاربر با این نام ایدی وجود ندارد.')
        elif not user.is_staff:
            raise ValidationError(u'کاربر معمولی نمی‌تواند نقش داشته باشد.')
        role = Role.objects.filter(id=attrs.get('role')).first()
        if not role:
            raise ValidationError(u'نقشی با این ایدی وجود ندارد')
        attrs.update({
            'user': user,
            'role': role
        })
        return attrs

    def create(self, data):
        try:
            user_role = UserRole.objects.create(**data)
        except:
            raise ValidationError('خطا در ساخت نقش')
        return user_role


class ChangePasswordSerializer(serializers.Serializer):
    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

    def validate(self, attrs):
        user = self.context.get('request').user
        if not user.check_password(attrs.get('old_password')):
            raise ValidationError('پسورد کنونی را اشتباه وارد کرده اید.')
        return attrs


class AdminUserUpdateSerializer(serializers.ModelSerializer):
    user_type = serializers.ChoiceField(choices=USER_TYPES)

    class Meta:
        model = User
        exclude = ['date_joined', 'id', 'password', 'is_staff', 'is_active', 'is_superuser', 'groups',
                   'user_permissions', 'last_login']

    def validate(self, attrs):
        user_type = attrs.get('user_type')
        if user_type:
            u_type = UserType.objects.filter(type_of=user_type).first()
            attrs['user_type'] = u_type
        return attrs


class OwnerUserUpdateSerializer(serializers.ModelSerializer):
    user_type = serializers.ChoiceField(choices=USER_TYPES)
    old_password = serializers.CharField(required=False)
    new_password = serializers.CharField(required=False)
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'mobile', 'address', 'gender', 'user_type', 'national_id',
                  'avatar','birth_date', 'old_password', 'new_password', 'university']

    def validate(self, attrs):
        old_pass = attrs.get('old_password')
        new_pass = attrs.get('new_password')
        if old_pass and not new_pass:
            raise ValidationError(u'رمز جدید را وارد کنید یا فیلد رمز را خالی بگذارید')
        if new_pass and not old_pass:
            raise ValidationError(u'رمز فعلی را وارد کنید یا فیلد رمز را خالی بگذارید.')
        if old_pass and new_pass:
            user = self.context.get('request').user
            if not user.check_password(old_pass):
                raise ValidationError('پسورد کنونی را اشتباه وارد کرده اید.')
            attrs.pop('old_password')
            new_pass = attrs.pop('new_password')
            attrs['password'] = new_pass
        user_type = attrs.get('user_type')
        if user_type:
            u_type = UserType.objects.filter(type_of=user_type).first()
            attrs['user_type'] = u_type
        return attrs


class PasswordResetSerializer(serializers.Serializer):
    user = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True, min_length=4)

    def validate(self, attrs):
        user = User.objects.filter(username=attrs.get('user')).first()
        if not user:
            raise ValidationError(u'کاربر مورد نظر یافت نشد.')
        elif user.is_superuser:
            raise ValidationError(u'کاربر مورد نظر یافت نشد.')
        attrs['user'] = user
        return attrs

class AvatarChangeSerializer(serializers.Serializer):
    avatar = serializers.FileField(required=True)


class UserTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserType
        fields = '__all__'
        read_only_fields = ['id', 'type_of']