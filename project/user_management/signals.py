# -*- coding: utf-8 -*-
from django.db.models.signals import post_save
from .models import User
from fcm_django.models import FCMDevice


def send_user_registration_notification(sender, **kwargs):
    user = kwargs["instance"]
    if kwargs["created"]:
        if not user.is_staff:
            data = {"full_name": user.get_full_name(), "user_id": user.id, "type": "register", "username": user.username}
            users = User.objects.filter(is_superuser=True)
            staffs = User.objects.filter(
                user_role__role__permissions__name__in=["view_user_books", "manage_user_books"])
            devices = FCMDevice.objects.filter(user__in=users)
            devices.send_message(title=u"درخواست عضویت", body=u"کاربر ثبت نام کرده است", data=data)
            devices = FCMDevice.objects.filter(user__in=staffs)
            devices.send_message(title=u"درخواست عضویت", body=u"کاربر ثبت نام کرده است", data=data)


post_save.connect(send_user_registration_notification, sender=User)
