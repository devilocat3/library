# -*- coding: utf-8 -*-
from rest_framework.viewsets import GenericViewSet
from rest_framework import status
from rest_framework import mixins
from rest_framework.parsers import MultiPartParser, FormParser, JSONParser
from rest_framework.decorators import action
from rest_framework_jwt.serializers import jwt_payload_handler, jwt_encode_handler
from rest_framework_jwt.views import jwt_response_payload_handler, ObtainJSONWebToken
from rest_framework_jwt.settings import api_settings
from rest_framework.permissions import AllowAny, IsAuthenticated
from datetime import datetime

from book.models import Book
from book.utils import get_first_queue_reserve
from logger.models import Log, ServerLoginLogs
from user_management.utils import convert_persian_digit
from .responses import CustomResponse
from .serializers import *
from .permissions import *
from .filters import UserFilters, UserRoleFilters, StaffFilters


class UserViewSet(GenericViewSet):
    serializer_class = UserSerializer
    parser_classes = [MultiPartParser, FormParser, JSONParser]
    queryset = User.objects.all()
    filter_class = UserFilters
    needed_permissions = ['manage_users']
    permission_classes = [IsAdminOrHasPermission]

    def get_queryset(self):
        q = User.objects.exclude(username='AnonymousUser')
        return q

    @action(methods=['patch'], url_path='update', detail=False, serializer_class=OwnerUserUpdateSerializer,
            permission_classes=[IsAuthenticated])
    def update_owner(self, request, **kwargs):
        """
        Update the current logged in user info based on given data
        """
        partial = True
        instance = request.user
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        username = serializer.validated_data.get('username')
        if username:
            Log.objects.update_log_user(instance.username, username)
        obj = serializer.save()
        password = serializer.validated_data.get('password')
        if password:
            obj.set_password(password)
            obj.save()
        serializer = UserSerializer(obj, context={'request': request})

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return CustomResponse(serializer.data)

    @action(methods=['post'], detail=False, url_path='avatar', serializer_class=AvatarChangeSerializer,
            permission_classes=[IsAuthenticated])
    def change_avatar(self, request, **kwargs):
        """
        changes the avatar of the current logged in user
        :param request:
        :param kwargs:
        :return:
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        request.user.avatar = serializer.validated_data['avatar']
        request.user.save()
        data = {
            'avatar': request.build_absolute_uri(request.user.avatar.url),
            'username': request.user.username,
            'id': request.user.id,
        }
        return CustomResponse(data)

    @action(methods=['patch'], url_path='admin/update', detail=True, serializer_class=AdminUserUpdateSerializer)
    def user_update(self, request, pk, **kwargs):
        """
        Updates a given user's info, the user is determind by it's id in the url! admin and staff access only!
        user the other update api for changing logged in user's info
        """
        partial = True
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        username = serializer.validated_data.get('username')
        if username:
            Log.objects.update_log_user(instance.username, username)
        obj = serializer.save()
        serializer = UserSerializer(obj, context={'request': request})
        manager = u'کارمند'
        if request.user.is_superuser:
            manager = u'مدیر'
        content = u'{} ،اطلاعات کاربر {} را تغییر داد'.format(manager, instance.username)
        Log.objects.create_log(content, request.user.username, request.user.is_staff, 'change')
        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return CustomResponse(serializer.data)

    @action(methods=['post'], url_path='change/password', detail=False, permission_classes=[IsAuthenticated],
            needed_permissions=[], serializer_class=ChangePasswordSerializer)
    def change_password(self, request, **kwargs):
        """
        Changes the current logged in user password
        """
        serializer = self.get_serializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        request.user.set_password(serializer.validated_data.get('new_password'))
        request.user.lookup_id += 1
        request.user.save()
        return CustomResponse(message=u'با موفقیت انجام شد.')

    @action(methods=['get'], url_path='list/staff', detail=False, needed_permissions=['view_user', 'manage_users'],
            filter_class=StaffFilters)
    def list_staff(self, request, **kwargs):
        """
        returns a list of staff users
        """
        queryset = self.filter_queryset(queryset=User.objects.filter(is_staff=True).exclude(is_superuser=True))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            resp = self.get_paginated_response(serializer.data)
            resp.data = {'data': resp.data, 'status': 0, 'message': None}
            return resp

        serializer = self.get_serializer(queryset, many=True)
        return CustomResponse(serializer.data)

    @action(methods=['get'], url_path='list', detail=False, needed_permissions=['view_user', 'manage_users'])
    def list_users(self, request, **kwargs):
        """
        returns a list of list of users with information

        """
        queryset = self.filter_queryset(self.get_queryset().exclude(is_staff=True))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            resp = self.get_paginated_response(serializer.data)
            resp.data = {'data': resp.data, 'status': 0, 'message': None}
            return resp

        serializer = self.get_serializer(queryset, many=True)
        return CustomResponse(serializer.data)

    @action(methods=['get'], url_path='detail', detail=True, permission_classes=[IsOwner],
            needed_permissions=['view_user', 'manage_users'])
    def user_detail(self, request, **kwargs):
        """
            returns detailed information about a given user
        """
        instance = self.get_object()
        if not instance:
            raise ValidationError('کاربر یافت نشد.')
        serializer = self.get_serializer(instance)
        return CustomResponse(serializer.data)

    @action(methods=['post'], url_path='register', detail=False, permission_classes=[AllowAny])
    def register(self, request, **kwargs):
        """
        api to register new normal user, returns token and user details
        for sending user_types choices are : employee, faculty_member, masters, phd, bachelor
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        token = jwt_response_payload_handler(token, user, request)
        data = serializer.data
        data.update(token)
        return CustomResponse(data, status=status.HTTP_201_CREATED)

    @action(methods=['post'], serializer_class=StaffCreateSerializer, url_path='staff/create', detail=False,
            permission_classes=[IsAdminUser])
    def create_staff(self, request, **kwargs):
        """
        admin access only, use this api to create employees for the system and ad
        :param request:
        :param kwargs:
        :return:
        """
        data = request.data
        data.update({
            'user_type': 'employee'
        })
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        User.objects.create_staff(**serializer.data)
        return CustomResponse(serializer.data)

    @action(methods=['post'], url_path='activate', detail=False, serializer_class=ApproveSerializer)
    def approve_user(self, request, **kwargs):
        """use this api to approve(activate) or deactivate an user"""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            user = User.objects.get(username=serializer.data.get('username'))
        except:
            raise ValidationError('نام کاربری وارد شده معتبر نمی‌باشد.')
        activate = serializer.data.get('activate')
        manager = u'کارمند'
        if request.user.is_superuser:
            manager = u'مدیر'
        if activate:
            content = u'{} ،حساب کاربر {} را فعال کرد'.format(manager, user.username)
            log_type = 'activate'
            user.activate()
        else:
            content = u'{} ،حساب کاربر {} را غیرفعال کرد'.format(manager, user.username)
            log_type = 'deactivate'
            user.deactivate()
        Log.objects.create_log(content, request.user.username, request.user.is_staff, log_type)
        return CustomResponse()

    def destroy(self, request, *args, **kwargs):
        """
        removes a user from database
        """
        instance = self.get_object()
        reserves = instance.books.filter(status='approved')
        if reserves:
            msg = u'کاربر {} کتاب در اختیار دارد که باید بر گرداند'.format(reserves.count())
            raise ValidationError(msg)
        reserves_pending = instance.books.filter(status='pending')
        for reserve in reserves_pending:
            book = Book.objects.filter(slug=reserve.book).first()
            book.return_book(1)
            get_first_queue_reserve(book.slug)
        instance.delete()
        manager = u'کارمند'
        if request.user.is_superuser:
            manager = u'مدیر'
        content = u'{} ،حساب کاربر {} را حذف کرد'.format(manager, instance.username)
        Log.objects.create_log(content, request.user.username, request.user.is_staff, 'delete')
        return CustomResponse(message='با موفقیت کاربر حذف شد.', status=status.HTTP_200_OK)

    @action(methods=['get'], url_path='detail', detail=False, permission_classes=[IsAuthenticated])
    def get_user_detail(self, request, **kwargs):
        """
        Returns the detail info of the logged in user!
        :param request:
        :param kwargs:
        :return:
        """
        serializer = self.get_serializer(request.user)
        return CustomResponse(serializer.data)

    @action(methods=['post'], url_path='reset/password', detail=False, serializer_class=PasswordResetSerializer)
    def reset_user_password(self, request, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data.get('user')
        user.set_password(serializer.validated_data.get('new_password'))
        user.save()
        serializer = UserSerializer(user, context={'request': request})
        return CustomResponse(serializer.data)


class NormalObtainJSONWebToken(ObtainJSONWebToken):
    def post(self, request, *args, **kwargs):
        data = {
            'username': convert_persian_digit(request.data.get('username')),
            'password': convert_persian_digit(request.data.get('password'))
        }
        serializer = self.get_serializer(data=data)

        if serializer.is_valid():
            user = serializer.object.get('user') or request.user
            ip = request.META.get('HTTP_CLIENT_IP', None)
            if ip:
                try:
                    ServerLoginLogs.objects.create(ip=ip, user=user)
                except:
                    print "Error in Creating ServerLogin!"
            if user.is_staff or user.is_superuser:
                raise ValidationError(u'نام کاربری یا رمز عبور نادرست است')
            token = serializer.object.get('token')
            user_serializer = UserSerializer(user, context={'request': request})
            response_data = jwt_response_payload_handler(token, user, request)
            response_data.update({'user_detail': user_serializer.data})
            response = CustomResponse(response_data)
            if api_settings.JWT_AUTH_COOKIE:
                expiration = (datetime.utcnow() +
                              api_settings.JWT_EXPIRATION_DELTA)
                response.set_cookie(api_settings.JWT_AUTH_COOKIE,
                                    token,
                                    expires=expiration,
                                    httponly=True)
            user.last_login = datetime.now()
            user.save()
            return response

        raise ValidationError(serializer.errors)


class CustomObtainJSONWebToken(ObtainJSONWebToken):
    """
    API View that receives a POST with a user's username and password.

    Returns a JSON Web Token that can be used for authenticated requests.
    """

    def post(self, request, *args, **kwargs):
        data = {
            'username': convert_persian_digit(request.data.get('username')),
            'password': convert_persian_digit(request.data.get('password'))
        }
        serializer = self.get_serializer(data=data)

        if serializer.is_valid():
            user = serializer.object.get('user') or request.user
            user_role = getattr(user, 'user_role', None)
            ip = request.META.get('HTTP_CLIENT_IP', None)
            if ip:
                try:
                    ServerLoginLogs.objects.create(ip=ip, user=user)
                except:
                    print "Error in Creating ServerLogin!"
            perms = []
            if user.is_superuser:
                permissions = RolePermission.objects.all()
                for perm in permissions:
                    perms.append(perm.name)
            elif user_role:
                permissions = user.user_role.get_perms()
                if permissions:
                    for perm in permissions:
                        perms.append(perm.name)
            token = serializer.object.get('token')
            user_serializer = UserSerializer(user, context={'request': request})
            response_data = jwt_response_payload_handler(token, user, request)
            response_data.update({'perms': perms,
                                  'user_detail': user_serializer.data})
            response = CustomResponse(response_data)
            if api_settings.JWT_AUTH_COOKIE:
                expiration = (datetime.utcnow() +
                              api_settings.JWT_EXPIRATION_DELTA)
                response.set_cookie(api_settings.JWT_AUTH_COOKIE,
                                    token,
                                    expires=expiration,
                                    httponly=True)
            user.last_login = datetime.now()
            user.save()
            return response

        raise ValidationError(serializer.errors)


class RoleManagementView(GenericViewSet):
    queryset = Role.objects.all()
    serializer_class = RoleSerializer
    permission_classes = [IsAdminUser]

    def perform_update(self, serializer):
        serializer.save()

    @action(methods=['get'], url_path='list/permissions', detail=False)
    def list_permissions(self, request, **kwargs):
        """
        Get a list of all Permissions. By default it's page size is 20 item per page. Defining new permissions is not
        available. Only through changes in database manually.
        """
        queryset = self.filter_queryset(queryset=RolePermission.objects.all())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = RolePermissionSerializer(page, many=True)
            resp = self.get_paginated_response(serializer.data)
            return CustomResponse(resp.data)

        serializer = RolePermissionSerializer(queryset, many=True)
        return CustomResponse(serializer.data)

    def list(self, reuqest, **kwargs):
        """
        Get a list of all roles. By default it's page size is 20 item per page.
        """
        queryset = self.filter_queryset(queryset=self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            resp = self.get_paginated_response(serializer.data)
            return CustomResponse(resp.data)

        serializer = self.get_serializer(queryset, many=True)
        return CustomResponse(serializer.data)

    def create(self, request, **kwargs):
        """
        Create a role. name is needed, permissions is optional. send permissions as permissions_list with a list of
        permission id's that you want o add to the role. ex: {"permissions_list":[1,2,3,...]}
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return CustomResponse(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        """
        Update a role by it's id, needs the whole data for update, permissions_list is the list of permission's ids
        to set for the role. ex: {"permissions_list":[1,2,3,...]}
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return CustomResponse(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        """
        Update a role by it's id, partially. just send the changed data to server. permissions_list is the list of
        permission's ids to set for the role. ex: {"permissions_list":[1,2,3,...]}
        """
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        """
        Remove a role from database by it's id in the url.
        """
        instance = self.get_object()
        instance.delete()
        return CustomResponse(message='با موفقیت حذف شد.', status=status.HTTP_200_OK)


class UserRoleView(GenericViewSet):
    queryset = UserRole.objects.all()
    serializer_class = UserRoleSerializer
    filter_class = UserRoleFilters
    permission_classes = [IsAdminUser]
    needed_permissions = []

    def list(self, reuqest, **kwargs):
        """
        Get a list of all user-roles. By default it's page size is 20 item per page.
        """
        queryset = self.filter_queryset(queryset=self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            resp = self.get_paginated_response(serializer.data)
            return CustomResponse(resp.data)

        serializer = self.get_serializer(queryset, many=True)
        return CustomResponse(serializer.data)

    @action(methods=['get'], url_path='detail', detail=True, permission_classes=[IsOwner])
    def get_user_role_detail(self, request, **kwargs):
        """
        Returns the details about a user role based on given id, admin access or owner!
        """
        instance = self.get_object()
        if not instance:
            raise ValidationError('کاربر یافت نشد.')
        serializer = self.get_serializer(instance)
        return CustomResponse(serializer.data)

    @action(methods=['post'], detail=False, url_path='upsert')
    def upsert_user_role(self, request, **kwargs):
        """
        Upsert api for UserRole: Input {user:id of the user you want to give the role to,
        role:id of the role you want to give the user}
        if the user does not have any role, it will create for him. if he already has a role,
        it will update the role for him.
        """
        serializer = UserRoleUpsertSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user_role = getattr(serializer.validated_data.get('user'), 'user_role', None)
        if user_role:
            user_role.role = serializer.validated_data.get('role')
            user_role.save()
        else:
            user_role = serializer.create(serializer.validated_data)
        msg = u'مدیر، نقش {} را به کاربر {} داد'.format(serializer.validated_data['role'].name,
                                                        serializer.validated_data['user'].username)
        Log.objects.create_log(msg, request.user.username, request.user.is_staff, 'change_role')
        serializer = self.get_serializer(user_role)
        return CustomResponse(serializer.data)

    def destroy(self, request, *args, **kwargs):
        """
        Remove a user's role from database by it's id in the url.
        """
        instance = self.get_object()
        instance.delete()
        return CustomResponse(message='با موفقیت حذف شد.', status=status.HTTP_200_OK)


class UserTypeViewSet(GenericViewSet, mixins.ListModelMixin):
    serializer_class = UserTypeSerializer
    queryset = UserType.objects.all()
    permission_classes = [IsAdminUser]

    def partial_update(self, request, *args, **kwargs):
        partial = True
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return CustomResponse(serializer.data)
