from django.apps import AppConfig


class UserConfig(AppConfig):
    name = 'user_management'

    def ready(self):
        from . import signals
        # # todo: why pre_migrate not working
        # from django.db.models.signals import pre_migrate
        # from django.contrib.auth import models as auth_models
        # from user_management.signals import add_user_permissions
        # pre_migrate.connect(add_user_permissions, sender=auth_models)
