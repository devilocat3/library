# -*- coding: utf-8 -*-
from django_filters import rest_framework as filters
from .models import Log, LOG_TYPE
from dashboard.jalali import convert_string_to_jdate

class LogFilters(filters.FilterSet):
    type = filters.ChoiceFilter(name="log_type", choices=LOG_TYPE)
    date_from = filters.CharFilter(method='custom_date_filter', name='created_at__gte')
    date_to = filters.CharFilter(method='custom_date_filter', name="created_at__lte")
    user = filters.CharFilter(name="user", lookup_expr="exact")
    staff = filters.BooleanFilter(name="is_staff")
    class Meta:
        model = Log
        fields = ['type', 'date_from', 'date_to', 'user']

    def custom_date_filter(self, qs, name, value):
        jtime = convert_string_to_jdate(value)
        qs = qs.filter(**{name: jtime})
        return qs