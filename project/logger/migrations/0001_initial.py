# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-27 10:27
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django_jalali.db.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Log',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content', models.TextField(max_length=200, verbose_name=b'\xd9\x84\xd8\xa7\xda\xaf')),
                ('log_type', models.CharField(choices=[(b'', b''), (b'activate', b'\xd9\x81\xd8\xb9\xd8\xa7\xd9\x84\xe2\x80\x8c\xda\xa9\xd8\xb1\xd8\xaf\xd9\x86'), (b'deactivate', b'\xd8\xba\xdb\x8c\xd8\xb1\xd9\x81\xd8\xb9\xd8\xa7\xd9\x84\xe2\x80\x8c\xda\xa9\xd8\xb1\xd8\xaf\xd9\x86'), (b'create', b'\xd8\xa7\xdb\x8c\xd8\xac\xd8\xa7\xd8\xaf \xd8\xb4\xd8\xaf\xd9\x87'), (b'reserve', b'\xd8\xaf\xd8\xb1\xd8\xae\xd9\x88\xd8\xa7\xd8\xb3\xd8\xaa \xd8\xb1\xd8\xb2\xd8\xb1\xd9\x88'), (b'approve', b'\xd8\xaa\xd8\xa7\xdb\x8c\xdb\x8c\xd8\xaf'), (b'canceled', b'\xd9\x84\xd8\xba\xd9\x88'), (b'delete', b'\xd8\xad\xd8\xb0\xd9\x81')], max_length=15, verbose_name=b'\xd9\x86\xd9\x88\xd8\xb9 \xd9\x84\xd8\xa7\xda\xaf')),
                ('created_at', django_jalali.db.models.jDateTimeField(blank=True, editable=False)),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-created_at',),
                'db_table': 'logger_log',
                'verbose_name': '\u0644\u0627\u06af',
                'verbose_name_plural': '\u0644\u0627\u06af',
            },
        ),
    ]
