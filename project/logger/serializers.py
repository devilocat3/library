# -*- coding: utf-8 -*-
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from django.contrib.auth import get_user_model
import datetime
from .models import Log

User = get_user_model()


class LogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Log
        fields = '__all__'

    def to_representation(self, instance):
        context = {'content': instance.content, 'id': instance.id}
        context['created_at'] = instance.created_at.strftime("%Y-%m-%d %H:%M:%S")
        context['log_type'] = instance.get_log_type_display()
        user = User.objects.filter(username=instance.user).first()
        if user:
            context['user'] = {
                'username': user.username,
                'id': user.id,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'is_staff': user.is_staff
            }
        else:
            context['user'] = {
                'username': instance.user,
                'is_staff': instance.is_staff,
                'id': None
            }
        return context


class LogDeleteSerializer(serializers.Serializer):
    log_ids = serializers.PrimaryKeyRelatedField(queryset=Log.objects.all(), many=True, required=True)
