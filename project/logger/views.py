# -*- coding: utf-8 -*-
from django.db.models import Q
from rest_framework.viewsets import GenericViewSet
from rest_framework.decorators import action

from user_management.permissions import IsAdminOrHasPermission
from user_management.responses import CustomResponse
from .serializers import LogSerializer, LogDeleteSerializer
from .filters import LogFilters
from .models import Log
from . import jalali


class LogViewSet(GenericViewSet):
    queryset = Log.objects.all()
    serializer_class = LogSerializer
    permission_classes = [IsAdminOrHasPermission]
    needed_permissions = ['view_logs', 'manage_logs']
    filter_class = LogFilters

    def get_queryset(self):
        if not self.request.user.is_superuser:
            qs = Log.objects.filter(Q(user=self.request.user)| Q(is_staff=False))
        else:
            qs = self.queryset
        return qs

    def list(self, request, **kwargs):
        """
        lists the logs
        :param request:
        :param kwargs:
        :return:
        """
        queryset = self.filter_queryset(queryset=self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            resp = self.get_paginated_response(serializer.data)
            return CustomResponse(resp.data)

        serializer = self.get_serializer(queryset, many=True)
        return CustomResponse(serializer.data)

    @action(methods=['post'], serializer_class=LogDeleteSerializer, url_path='delete', detail=False,
            needed_permissions=['manage_logs'])
    def delete_logs(self, request, **kwargs):
        """
        removes the given ids, logs
        :param request:
        :param kwargs:
        :return:
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        for item in serializer.validated_data['log_ids']:
            item.delete()
        return CustomResponse(message=u'با موفقیت حذف شدند.')
