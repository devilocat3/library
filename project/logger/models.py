# -*- coding: utf-8 -*-
from django.db import models

from user_management.models import User
from django_jalali.db import models as jmodels

LOG_TYPE = (
    ('', ''),
    ('activate', 'فعال‌کردن'),
    ('deactivate', 'غیرفعال‌کردن'),
    ('create', 'ایجاد'),
    ('reserve', 'درخواست رزرو'),
    ('approve', 'تایید'),
    ('canceled', 'لغو'),
    ('delete', 'حذف'),
    ('return', 'برگشت'),
    ('change', 'تغییر'),
    ('change_role', 'تغییرنقش'),
)


class LogManager(models.Manager):

    def create_log(self, content, username, staff, log_type=''):
        self.create(content=content, log_type=log_type, user=username, is_staff=staff)

    def update_log_user(self, user, new_user):
        logs = self.filter(user=user)
        for log in logs:
            log.user = new_user
            log.save()



class Log(models.Model):
    content = models.TextField('لاگ', max_length=200)
    user = models.CharField(max_length=150, null=True, blank=True)
    is_staff = models.BooleanField(default=False)
    # user = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)
    log_type = models.CharField('نوع لاگ', choices=LOG_TYPE, max_length=15)
    created_at = jmodels.jDateTimeField(auto_now_add=True)

    objects = LogManager()

    class Meta:
        db_table = 'logger_log'
        ordering = ('-created_at',)
        verbose_name_plural = 'لاگ'
        verbose_name = 'لاگ'

    def __unicode__(self):
        return self.content


class ServerLoginLogs(models.Model):
    ip = models.CharField(max_length=100)
    user = models.ForeignKey(User, related_name='logins')
    created = models.DateTimeField(auto_now_add=True)