from django.contrib import admin

from .models import Log, ServerLoginLogs


class ServerLogsAdmin(admin.ModelAdmin):
    list_display = ['user', 'ip', 'created']
    ordering = ['-created']
    search_fields = ['user__username']


admin.site.register(Log)
admin.site.register(ServerLoginLogs, ServerLogsAdmin)
