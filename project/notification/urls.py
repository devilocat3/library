from rest_framework.routers import DefaultRouter
from django.conf.urls import url, include
from .views import NotificationViewSet
router = DefaultRouter()

router.register(r'notification', NotificationViewSet)

urlpatterns = [
    url(r'', include(router.urls))
]