from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from .models import Notification


class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = '__all__'
        read_only_fields = ['id', 'created', 'is_read']
