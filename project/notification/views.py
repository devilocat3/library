# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.viewsets import GenericViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework import mixins
from rest_framework.decorators import action
from user_management.responses import CustomResponse
from user_management.permissions import IsAdminOrHasPermission
from .serializers import NotificationSerializer
from .models import Notification


class NotificationViewSet(GenericViewSet, mixins.CreateModelMixin, mixins.DestroyModelMixin):
    queryset = Notification.objects.all().select_related('receiver')
    serializer_class = NotificationSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return CustomResponse(serializer.data, headers=headers)

    def list(self, request, *args, **kwargs):
        if request.user.is_superuser:
            qs = self.queryset
        else:
            qs = Notification.objects.filter(receiver=request.user)
        queryset = self.filter_queryset(qs)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            response = self.get_paginated_response(serializer.data)
            return CustomResponse(response.data)

        serializer = self.get_serializer(queryset, many=True)
        return CustomResponse(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return CustomResponse()

    @action(methods=['get'], detail=False, url_path='unread')
    def get_unread_msgs(self, request, **kwargs):
        """
        returns the list of unread messages for the logged in user and set their status to read
        :param request:
        :param kwargs:
        :return:
        """
        queryset = Notification.objects.filter(receiver=request.user, is_read=False)
        page = self.paginate_queryset(queryset)
        for item in queryset:
            item.is_read = True
            item.save()
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            response = self.get_paginated_response(serializer.data)
            return CustomResponse(response.data)

        serializer = self.get_serializer(queryset, many=True)
        return CustomResponse(serializer.data)
