# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django_jalali.db import models as jmodels
from django.contrib.auth import get_user_model

User = get_user_model()


class Notification(models.Model):
    content = models.TextField()
    receiver = models.ForeignKey(User, related_name='notifications')
    created = jmodels.jDateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)


    def __unicode__(self):
        return self.receiver.username
